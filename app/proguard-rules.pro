##不进行混淆
-keepclasseswithmembers,allowshrinking class * {
    native <methods>;
}

# Keep our interfaces so they can be used by other ProGuard rules.
# See http://sourceforge.net/p/proguard/bugs/466/
-keep,allowobfuscation @interface android.support.annotation.Keep

# Do not strip any method/class that is annotated with @Keep
-keep @android.support.annotation.Keep class *
-keepclassmembers class * {
    @android.support.annotation.Keep *;
}

# ProGuard configurations for NetworkBench Lens
-keep class com.networkbench.** { *; }
-dontwarn com.networkbench.**
-keepattributes Exceptions, Signature, InnerClasses

#过滤泛型，防止强制类型转换异常
-keepattributes Signature

#用到反射和泛型，把泛型的全部过滤掉，否则model的set/get方法会全部被混淆，找不到方法，返回null，要把model下的所有类全部过滤掉，防止空指针异常

#Annotation
-keepattributes *Annotation*

# -- Android Annotations --
#参考 https://github.com/excilys/androidannotations/issues/1098
-dontwarn org.springframework.**

# OkHttp
-dontwarn rx.**
-dontwarn okio.**
-dontwarn com.squareup.okhttp.**
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }

-dontwarn java.nio.file.*
-dontwarn javax.annotation.**
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement


#js
-keepclassmembernames class com.youzan.fringe.scheme.** {*; }

# webview upload file
-keepclassmembers class * extends android.webkit.WebChromeClient {
   public void openFileChooser(...);
}

#富文本
-keep class com.youzan.hulkeditor.HulkRichEditor$JsBridge{*; }

#app upgrade
-keepclassmembernames class com.youzan.genesis.info.** {*; }

#Picasso
-keepattributes SourceFile,LineNumberTable
-keep class com.parse.*{ *; }
-dontwarn com.parse.**
-dontwarn com.squareup.picasso.**
-keepclasseswithmembernames class * {
    native <methods>;
}

#api 23
-dontwarn org.apache.http.**
-dontwarn android.net.**
-dontwarn com.google.android.gms.**
-dontwarn com.android.volley.toolbox.**
-dontwarn android.util.FloatMath.**

#android
-dontwarn android.content.res.*
-keep class android.support.v4.** {*; }
-keep interface android.support.v4.** {*; }
-dontwarn android.support.v4.**
-keep class android.support.v7.** {*; }
-keep interface android.support.v7.** {*; }
-dontwarn android.support.v7.**
-keep class android.support.v13.** {*; }
-keep interface android.support.v13.** {*; }
-dontwarn android.support.v13.**
-keep class android.support.design.**{*; }
-keep interface android.support.design.** { *; }
-keep public class android.support.design.R$* { *; }
-dontwarn android.support.design.**

#Retrofit
-dontwarn okio.**
-dontwarn retrofit2.Platform$Java8