package com.yinghe.eclass.medium.util;

import java.text.DecimalFormat;
import java.util.regex.Pattern;

public class DigitUtil {
    public static String getPriceFormat(double price) {
        return new DecimalFormat("#0.00").format(price);
    }

    public static boolean isNumeric(String str) {
        if (null == str) {
            return false;
        }

        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static boolean isMobileNumber(String str) {
        return Pattern.compile("^1[0-9]{10}$").matcher(str).find();
    }

    public static boolean isQQNumber(String str) {
        return Pattern.compile("^[1-9][0-9]{4,15}$").matcher(str).find();
    }

    public static String getFloatDefaultFormat(float data) {
        return new DecimalFormat("#0.00").format(data);
    }

    public static String getDoubleDefaultFormat(double data) {
        return new DecimalFormat("#0.00").format(data);
    }

    public static String getIntegerCommaFormat(int data) {
        return new DecimalFormat("#,###").format(data);
    }

    public static String getFloatCommaFormat(float data) {
        return new DecimalFormat("#,##0.00").format(data);
    }
}
