package com.yinghe.eclass.medium.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

import com.yinghe.eclass.R;

public class ProgressWheel extends View {
    private static final String TAG = ProgressWheel.class.getSimpleName();
    private final int mBarLength = 16;
    private final int mBarMaxLength = 270;
    private final long mPauseGrowingTime = 200;
    //Sizes (with defaults in DP)
    private int mCircleRadius = 28;
    private int mBarWidth = 4;
    private int mRimWidth = 4;
    private boolean mFillRadius = false;
    private double mTimeStartGrowing = 0;
    private double mBarSpinCycleTime = 460;
    private float mBarExtraLength = 0;
    private boolean mBarGrowingFromFront = true;
    private long mPausedTimeWithoutGrowing = 0;
    //Colors (with defaults)
    private int mBarColor = 0xAA000000;
    private int mRimColor = 0x00FFFFFF;

    //Paints
    private Paint mBarPaint = new Paint();
    private Paint mRimPaint = new Paint();

    //Rectangles
    private RectF mCircleBounds = new RectF();

    //Animation
    //The amount of degrees per second
    private float mSpinSpeed = 230.0f;
    //private float mSpinSpeed = 120.0f;
    // The last time the spinner was animated
    private long mLastTimeAnimated = 0;

    private boolean mLinearProgress;

    private float mProgress = 0.0f;
    private float mTargetProgress = 0.0f;
    private boolean mIsSpinning = false;

    /**
     * The constructor for the ProgressWheel
     *
     * @param context
     * @param attrs
     */
    public ProgressWheel(Context context, AttributeSet attrs) {
        super(context, attrs);

        parseAttributes(context.obtainStyledAttributes(attrs,
                R.styleable.ProgressWheel));
    }

    /**
     * The constructor for the ProgressWheel
     *
     * @param context
     */
    public ProgressWheel(Context context) {
        super(context);
    }

    //----------------------------------
    //Setting up stuff
    //----------------------------------

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int viewWidth = mCircleRadius + this.getPaddingLeft() + this.getPaddingRight();
        int viewHeight = mCircleRadius + this.getPaddingTop() + this.getPaddingBottom();

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

        //Measure Width
        if (widthMode == MeasureSpec.EXACTLY) {
            //Must be this size
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            width = Math.min(viewWidth, widthSize);
        } else {
            //Be whatever you want
            width = viewWidth;
        }

        //Measure Height
        if (heightMode == MeasureSpec.EXACTLY || widthMode == MeasureSpec.EXACTLY) {
            //Must be this size
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            height = Math.min(viewHeight, heightSize);
        } else {
            //Be whatever you want
            height = viewHeight;
        }

        setMeasuredDimension(width, height);
    }

    /**
     * Use onSizeChanged instead of onAttachedToWindow to get the dimensions of the view,
     * because this method is called after measuring the dimensions of MATCH_PARENT & WRAP_CONTENT.
     * Use this dimensions to setup the bounds and paints.
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        setupBounds(w, h);
        setupPaints();
        invalidate();
    }

    /**
     * Set the properties of the paints we're using to
     * draw the progress wheel
     */
    private void setupPaints() {
        mBarPaint.setColor(mBarColor);
        mBarPaint.setAntiAlias(true);
        mBarPaint.setStyle(Paint.Style.STROKE);
        mBarPaint.setStrokeWidth(mBarWidth);

        mRimPaint.setColor(mRimColor);
        mRimPaint.setAntiAlias(true);
        mRimPaint.setStyle(Paint.Style.STROKE);
        mRimPaint.setStrokeWidth(mRimWidth);
    }

    /**
     * Set the bounds of the component
     */
    private void setupBounds(int layout_width, int layout_height) {
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();

        if (!mFillRadius) {
            // Width should equal to Height, find the min value to setup the circle
            int minValue = Math.min(layout_width - paddingLeft - paddingRight,
                    layout_height - paddingBottom - paddingTop);

            int circleDiameter = Math.min(minValue, mCircleRadius * 2 - mBarWidth * 2);

            // Calc the Offset if needed for centering the wheel in the available space
            int xOffset = (layout_width - paddingLeft - paddingRight - circleDiameter) / 2
                    + paddingLeft;
            int yOffset = (layout_height - paddingTop - paddingBottom - circleDiameter) / 2
                    + paddingTop;

            mCircleBounds = new RectF(xOffset + mBarWidth,
                    yOffset + mBarWidth,
                    xOffset + circleDiameter - mBarWidth,
                    yOffset + circleDiameter - mBarWidth);
        } else {
            mCircleBounds = new RectF(paddingLeft + mBarWidth,
                    paddingTop + mBarWidth,
                    layout_width - paddingRight - mBarWidth,
                    layout_height - paddingBottom - mBarWidth);
        }
    }

    /**
     * Parse the attributes passed to the view from the XML
     *
     * @param a the attributes to parse
     */
    private void parseAttributes(TypedArray a) {
        // We transform the default values from DIP to pixels
        DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
        mBarWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, mBarWidth,
                metrics);
        mRimWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, mRimWidth,
                metrics);
        mCircleRadius = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, mCircleRadius,
                metrics);

        mCircleRadius = (int) a.getDimension(R.styleable.ProgressWheel_circleRadius, mCircleRadius);

        mFillRadius = a.getBoolean(R.styleable.ProgressWheel_fillRadius, false);

        mBarWidth = (int) a.getDimension(R.styleable.ProgressWheel_barWidth, mBarWidth);

        mRimWidth = (int) a.getDimension(R.styleable.ProgressWheel_rimWidth, mRimWidth);

        float baseSpinSpeed = a.getFloat(R.styleable.ProgressWheel_spinSpeed, mSpinSpeed / 360.0f);
        mSpinSpeed = baseSpinSpeed * 360;

        mBarSpinCycleTime = a.getInt(R.styleable.ProgressWheel_barSpinCycleTime, (int)
                mBarSpinCycleTime);

        mBarColor = a.getColor(R.styleable.ProgressWheel_barColor, mBarColor);

        mRimColor = a.getColor(R.styleable.ProgressWheel_rimColor, mRimColor);

        mLinearProgress = a.getBoolean(R.styleable.ProgressWheel_linearProgress, false);

        if (a.getBoolean(R.styleable.ProgressWheel_progressIndeterminate, false)) {
            spin();
        }

        // Recycle
        a.recycle();
    }

    //----------------------------------
    //Animation stuff
    //----------------------------------

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawArc(mCircleBounds, 360, 360, false, mRimPaint);

        boolean mustInvalidate = false;

        if (mIsSpinning) {
            //Draw the spinning bar
            mustInvalidate = true;

            long deltaTime = (SystemClock.uptimeMillis() - mLastTimeAnimated);
            float deltaNormalized = deltaTime * mSpinSpeed / 1000.0f;

            updateBarLength(deltaTime);

            mProgress += deltaNormalized;
            if (mProgress > 360) {
                mProgress -= 360f;
            }
            mLastTimeAnimated = SystemClock.uptimeMillis();

            float from = mProgress - 90;
            float length = mBarLength + mBarExtraLength;

            canvas.drawArc(mCircleBounds, from, length, false,
                    mBarPaint);
        } else {
            if (mProgress != mTargetProgress) {
                //We smoothly increase the progress bar
                mustInvalidate = true;

                float deltaTime = (float) (SystemClock.uptimeMillis() - mLastTimeAnimated) / 1000;
                float deltaNormalized = deltaTime * mSpinSpeed;

                mProgress = Math.min(mProgress + deltaNormalized, mTargetProgress);
                mLastTimeAnimated = SystemClock.uptimeMillis();
            }

            float offset = 0.0f;
            float progress = mProgress;
            if (!mLinearProgress) {
                float factor = 2.0f;
                offset = (float) (1.0f - Math.pow(1.0f - mProgress / 360.0f, 2.0f * factor))
                        * 360.0f;
                progress = (float) (1.0f - Math.pow(1.0f - mProgress / 360.0f, factor)) * 360.0f;
            }

            canvas.drawArc(mCircleBounds, offset - 90, progress, false, mBarPaint);
        }

        if (mustInvalidate) {
            invalidate();
        }
    }

    private void updateBarLength(long deltaTimeInMilliSeconds) {
        if (mPausedTimeWithoutGrowing >= mPauseGrowingTime) {
            mTimeStartGrowing += deltaTimeInMilliSeconds;

            if (mTimeStartGrowing > mBarSpinCycleTime) {
                // We completed a size change cycle
                // (growing or shrinking)
                mTimeStartGrowing -= mBarSpinCycleTime;
                //if(mBarGrowingFromFront) {
                mPausedTimeWithoutGrowing = 0;
                //}
                mBarGrowingFromFront = !mBarGrowingFromFront;
            }

            float distance = (float) Math.cos((mTimeStartGrowing / mBarSpinCycleTime + 1) * Math
                    .PI) / 2 + 0.5f;
            float destLength = (mBarMaxLength - mBarLength);

            if (mBarGrowingFromFront) {
                mBarExtraLength = distance * destLength;
            } else {
                float newLength = destLength * (1 - distance);
                mProgress += (mBarExtraLength - newLength);
                mBarExtraLength = newLength;
            }
        } else {
            mPausedTimeWithoutGrowing += deltaTimeInMilliSeconds;
        }
    }

    /**
     * Check if the wheel is currently spinning
     */

    public boolean mIsSpinning() {
        return mIsSpinning;
    }

    /**
     * Reset the count (in increment mode)
     */
    public void resetCount() {
        mProgress = 0.0f;
        mTargetProgress = 0.0f;
        invalidate();
    }

    /**
     * Turn off spin mode
     */
    public void stopSpinning() {
        mIsSpinning = false;
        mProgress = 0.0f;
        mTargetProgress = 0.0f;
        invalidate();
    }

    /**
     * Puts the view on spin mode
     */
    public void spin() {
        mLastTimeAnimated = SystemClock.uptimeMillis();
        mIsSpinning = true;
        invalidate();
    }

    /**
     * Set the progress to a specific value,
     * the bar will be set instantly to that value
     *
     * @param progress the progress between 0 and 1
     */
    public void setInstantProgress(float progress) {
        if (mIsSpinning) {
            mProgress = 0.0f;
            mIsSpinning = false;
        }

        if (progress > 1.0f) {
            progress -= 1.0f;
        } else if (progress < 0) {
            progress = 0;
        }

        if (progress == mTargetProgress) {
            return;
        }

        mTargetProgress = Math.min(progress * 360.0f, 360.0f);
        mProgress = mTargetProgress;
        mLastTimeAnimated = SystemClock.uptimeMillis();
        invalidate();
    }

    // Great way to save a view's state http://stackoverflow.com/a/7089687/1991053
    @Override
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();

        WheelSavedState ss = new WheelSavedState(superState);

        // We save everything that can be changed at runtime
        ss.mProgress = this.mProgress;
        ss.mTargetProgress = this.mTargetProgress;
        ss.mIsSpinning = this.mIsSpinning;
        ss.mSpinSpeed = this.mSpinSpeed;
        ss.mBarWidth = this.mBarWidth;
        ss.mBarColor = this.mBarColor;
        ss.mRimWidth = this.mRimWidth;
        ss.mRimColor = this.mRimColor;
        ss.mCircleRadius = this.mCircleRadius;
        ss.mLinearProgress = this.mLinearProgress;
        ss.mFillRadius = this.mFillRadius;

        return ss;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        if (!(state instanceof WheelSavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }

        WheelSavedState ss = (WheelSavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());

        this.mProgress = ss.mProgress;
        this.mTargetProgress = ss.mTargetProgress;
        this.mIsSpinning = ss.mIsSpinning;
        this.mSpinSpeed = ss.mSpinSpeed;
        this.mBarWidth = ss.mBarWidth;
        this.mBarColor = ss.mBarColor;
        this.mRimWidth = ss.mRimWidth;
        this.mRimColor = ss.mRimColor;
        this.mCircleRadius = ss.mCircleRadius;
        this.mLinearProgress = ss.mLinearProgress;
        this.mFillRadius = ss.mFillRadius;
    }

    /**
     * @return the current progress between 0.0 and 1.0,
     * if the wheel is indeterminate, then the result is -1
     */
    public float getProgress() {
        return mIsSpinning ? -1 : mProgress / 360.0f;
    }

    //----------------------------------
    //Getters + setters
    //----------------------------------

    /**
     * Set the progress to a specific value,
     * the bar will smoothly animate until that value
     *
     * @param progress the progress between 0 and 1
     */
    public void setProgress(float progress) {
        if (mIsSpinning) {
            mProgress = 0.0f;
            mIsSpinning = false;
        }

        if (progress > 1.0f) {
            progress -= 1.0f;
        } else if (progress < 0) {
            progress = 0;
        }

        if (progress == mTargetProgress) {
            return;
        }

        // If we are currently in the right position
        // we set again the last time animated so the
        // animation starts smooth from here
        if (mProgress == mTargetProgress) {
            mLastTimeAnimated = SystemClock.uptimeMillis();
        }

        mTargetProgress = Math.min(progress * 360.0f, 360.0f);

        invalidate();
    }

    /**
     * Sets the determinate progress mode
     *
     * @param isLinear if the progress should increase linearly
     */
    public void setLinearProgress(boolean isLinear) {
        mLinearProgress = isLinear;
        if (!mIsSpinning) {
            invalidate();
        }
    }

    /**
     * @return the radius of the wheel in pixels
     */
    public int getCircleRadius() {
        return mCircleRadius;
    }

    /**
     * Sets the radius of the wheel
     *
     * @param mCircleRadius the expected radius, in pixels
     */
    public void setCircleRadius(int mCircleRadius) {
        this.mCircleRadius = mCircleRadius;
        if (!mIsSpinning) {
            invalidate();
        }
    }

    /**
     * @return the width of the spinning bar
     */
    public int getBarWidth() {
        return mBarWidth;
    }

    /**
     * Sets the width of the spinning bar
     *
     * @param mBarWidth the spinning bar width in pixels
     */
    public void setBarWidth(int mBarWidth) {
        this.mBarWidth = mBarWidth;
        if (!mIsSpinning) {
            invalidate();
        }
    }

    /**
     * @return the color of the spinning bar
     */
    public int getBarColor() {
        return mBarColor;
    }

    /**
     * Sets the color of the spinning bar
     *
     * @param mBarColor The spinning bar color
     */
    public void setBarColor(int mBarColor) {
        this.mBarColor = mBarColor;
        setupPaints();
        if (!mIsSpinning) {
            invalidate();
        }
    }

    /**
     * @return the color of the wheel's contour
     */
    public int getRimColor() {
        return mRimColor;
    }

    /**
     * Sets the color of the wheel's contour
     *
     * @param mRimColor the color for the wheel
     */
    public void setRimColor(int mRimColor) {
        this.mRimColor = mRimColor;
        setupPaints();
        if (!mIsSpinning) {
            invalidate();
        }
    }

    /**
     * @return the base spinning speed, in full circle turns per second
     * (1.0 equals on full turn in one second), this value also is applied for
     * the smoothness when setting a progress
     */
    public float getSpinSpeed() {
        return mSpinSpeed / 360.0f;
    }

    /**
     * Sets the base spinning speed, in full circle turns per second
     * (1.0 equals on full turn in one second), this value also is applied for
     * the smoothness when setting a progress
     *
     * @param mSpinSpeed the desired base speed in full turns per second
     */
    public void setSpinSpeed(float mSpinSpeed) {
        this.mSpinSpeed = mSpinSpeed * 360.0f;
    }

    /**
     * @return the width of the wheel's contour in pixels
     */
    public int getRimWidth() {
        return mRimWidth;
    }

    /**
     * Sets the width of the wheel's contour
     *
     * @param mRimWidth the width in pixels
     */
    public void setRimWidth(int mRimWidth) {
        this.mRimWidth = mRimWidth;
        if (!mIsSpinning) {
            invalidate();
        }
    }

    public void showProgressBar() {
        setVisibility(VISIBLE);
    }

    public void hideProgressBar() {
        setVisibility(GONE);
    }

    static class WheelSavedState extends BaseSavedState {
        //required field that makes Parcelables from a Parcel
        public static final Creator<WheelSavedState> CREATOR =
                new Creator<WheelSavedState>() {
                    public WheelSavedState createFromParcel(Parcel in) {
                        return new WheelSavedState(in);
                    }

                    public WheelSavedState[] newArray(int size) {
                        return new WheelSavedState[size];
                    }
                };
        float mProgress;
        float mTargetProgress;
        boolean mIsSpinning;
        float mSpinSpeed;
        int mBarWidth;
        int mBarColor;
        int mRimWidth;
        int mRimColor;
        int mCircleRadius;
        boolean mLinearProgress;
        boolean mFillRadius;

        WheelSavedState(Parcelable superState) {
            super(superState);
        }

        private WheelSavedState(Parcel in) {
            super(in);
            this.mProgress = in.readFloat();
            this.mTargetProgress = in.readFloat();
            this.mIsSpinning = in.readByte() != 0;
            this.mSpinSpeed = in.readFloat();
            this.mBarWidth = in.readInt();
            this.mBarColor = in.readInt();
            this.mRimWidth = in.readInt();
            this.mRimColor = in.readInt();
            this.mCircleRadius = in.readInt();
            this.mLinearProgress = in.readByte() != 0;
            this.mFillRadius = in.readByte() != 0;
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeFloat(this.mProgress);
            out.writeFloat(this.mTargetProgress);
            out.writeByte((byte) (mIsSpinning ? 1 : 0));
            out.writeFloat(this.mSpinSpeed);
            out.writeInt(this.mBarWidth);
            out.writeInt(this.mBarColor);
            out.writeInt(this.mRimWidth);
            out.writeInt(this.mRimColor);
            out.writeInt(this.mCircleRadius);
            out.writeByte((byte) (mLinearProgress ? 1 : 0));
            out.writeByte((byte) (mFillRadius ? 1 : 0));
        }
    }
}
