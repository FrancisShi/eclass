package com.yinghe.eclass.medium.http;

import com.yinghe.eclass.business.entity.ChooseClassroom;
import com.yinghe.eclass.business.entity.ClassStudent;
import com.yinghe.eclass.business.entity.ClassroomGroup;
import com.yinghe.eclass.business.entity.ClassroomPride;
import com.yinghe.eclass.business.entity.ClassroomSelfStatic;
import com.yinghe.eclass.business.entity.CourseCat;
import com.yinghe.eclass.business.entity.CourseClassWork;
import com.yinghe.eclass.business.entity.CourseNote;
import com.yinghe.eclass.business.entity.ExamItem;
import com.yinghe.eclass.business.entity.HomeWorkSubmit;
import com.yinghe.eclass.business.entity.Homework;
import com.yinghe.eclass.business.entity.HomeworkDetail;
import com.yinghe.eclass.business.entity.UserLogin;

import java.io.File;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;
import rx.Observable;

public interface ClassService {

    /**
     * login
     */
    @GET("checkLogin.do")
    Observable<HttpResult<UserLogin>> login(@Query("username") String name, @Query("password")
            String
            password);

    /**
     * course
     */
    @GET("api/student/course/list/{id}")
    Observable<HttpResult<List<CourseCat>>> course(@Path("id") String id);

    // 傻逼 resource
    @GET("api/student/course/course/recourses/{id}")
    Observable<HttpResult<List<CourseNote>>> courseNote(@Path("id") String id);

    // 1为课后，2为随堂
    @GET("api/student/homework/list/{id}/1")
    Observable<HttpResult<List<CourseClassWork>>> courseClass(@Path("id") String id);

    // TODO: 16/6/24 test
    @GET("api/student/course/cy/cy/a320b07f82924af0995103c847437888/50f9c0f3e5a641bf9098631cfce9f970/3cbff1d39e1a42b4815ba7414bb6fb6c")
    Observable<HttpResult<List<ExamItem>>> courseExamList();

    /**
     * classroom
     */
    @GET("api/teacher/class/praiseRank/coordinate/{classId}")
    Observable<HttpResult<List<ClassroomPride>>> classroomPride(@Path("classId") String classId);

    @GET("api/teacher/praise/statistics/{userId}")
    Observable<HttpResult<ClassroomSelfStatic>> classroomSelf(@Path("userId") String userId);


    @GET("api/set/group/my/group/{teacherId}/{classId}/{studentId}")
    Observable<HttpResult<List<ClassroomGroup>>> classroomGroup(@Path("teacherId") String teacherId,
                                                                @Path("classId") String classId,
                                                                @Path("studentId") String
                                                                        studentId);

    /**
     * homework
     * homeworkType 1为课后，2为随堂
     */
    @GET("api/student/homework/list/{userId}/1/{orderBy}")
    Observable<HttpResult<List<Homework>>> homeworkList(@Path("userId") String userId,
                                                        @Path("orderBy") String orderBy);

    @GET("api/student/homework/homework/{userId}/{homeworkId}")
    Observable<HttpResult<HomeworkDetail>> homeworkDetail(@Path("userId") String userId,
                                                          @Path("homeworkId") String homeworkId);

    /**
     * 教师端
     */
    @GET("api/teacher/list/class/{userId}")
    Observable<HttpResult<List<ChooseClassroom>>> chooseClassroom(@Path("userId") String userId);

    @GET("api/student/myClass/students/{classId}")
    Observable<HttpResult<List<ClassStudent>>> getStudentList(@Path("classId") String classId);

}

