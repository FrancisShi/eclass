package com.yinghe.eclass.medium.web;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class SimpleWebView extends WebView {

    public SimpleWebView(Context context) {
        super(context);
        init(context);
        blockJsInterface();
    }

    public SimpleWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
        blockJsInterface();
    }

    private void init(Context context) {
        initWebviewSettings(context);
    }

    private void initWebviewSettings(Context context) {
        final WebSettings webSettings = this.getSettings();

        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setAppCacheEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadsImagesAutomatically(true);

        //支持通过js打开新的窗口
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);

        //关闭浏览器记住密码功能(否则会显示一个弹框)
        webSettings.setSavePassword(false);
        webSettings.setSaveFormData(false);

        //启动local storage
        webSettings.setDomStorageEnabled(true);
        webSettings.setDatabaseEnabled(true);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            webSettings.setDatabasePath("/data/data/" + this.getContext().getPackageName()
                    + "/databases/");
        }
        //gps定位
        webSettings.setGeolocationEnabled(true);
        webSettings.setGeolocationDatabasePath(context.getFilesDir().getPath());

        webSettings.setBuiltInZoomControls(false);
        //enablePageZoom(settings);
    }

    /**
     * 自动缩放
     */
    private void enablePageZoom(WebSettings settings) {
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            settings.setDisplayZoomControls(false);
        }
    }

    /**
     * 移除系统webkit内置的危险接口
     */
    private void blockJsInterface() {
        this.removeJavascriptInterface("searchBoxJavaBridge_");
        this.removeJavascriptInterface("accessibility");
        this.removeJavascriptInterface("accessibilityTraversal");
    }
}
