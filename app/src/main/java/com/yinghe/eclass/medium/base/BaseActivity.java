package com.yinghe.eclass.medium.base;

import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import butterknife.ButterKnife;
import butterknife.Unbinder;

import com.yinghe.eclass.R;
import com.yinghe.eclass.medium.component.ProgressWheel;

public class BaseActivity extends AppCompatActivity {

    private ViewGroup mRootView;
    private View mProgressView;
    private ProgressWheel mProgressBar;
    protected static boolean mIsProgressBarShown = false;
    private Unbinder mUnbinder;

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        mUnbinder = ButterKnife.bind(this);
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        mUnbinder = ButterKnife.bind(this);
    }

    public void showProgressBar() {
        if (mIsProgressBarShown) {
            return;
        }
        FrameLayout.LayoutParams mLayoutParams;
        if (mProgressView == null) {
            mProgressView = LayoutInflater.from(this).inflate(R.layout.layout_progress_view, null);
            mRootView = (ViewGroup) this.getWindow().getDecorView();

            mLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,
                    FrameLayout.LayoutParams.WRAP_CONTENT);
            mLayoutParams.gravity = Gravity.CENTER;
            mRootView.addView(mProgressView, mLayoutParams);
            mProgressBar = (ProgressWheel) mProgressView.findViewById(R.id
                    .activity_base_progressbar);
            mProgressBar.showProgressBar();
            mIsProgressBarShown = true;
        }
    }

    public void hideProgressBar() {
        if (mRootView != null && mProgressView != null) {
            if (null != mProgressBar) {
                mProgressBar.hideProgressBar();
            }
            mRootView.removeView(mProgressView);
        }

        mProgressView = null;
        mRootView = null;
        mIsProgressBarShown = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
