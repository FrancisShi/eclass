package com.yinghe.eclass.medium.http;

public interface SubscriberOnNextListener<T> {
    void onNext(T t);
}