package com.yinghe.eclass.medium.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yinghe.eclass.R;

public abstract class ButtonView extends RelativeLayout {

    private Context mContext;
    private TextView mTextView;
    private OnButtonClickListener mListener;

    public ButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.component_button_view, this, false);
        mTextView = (TextView) view.findViewById(R.id.button_view_textview);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ButtonView);
        mTextView.setText(typedArray.getString(R.styleable.ButtonView_buttonText));

        typedArray.recycle();

        addView(view);
    }

    public void setButtonTextTitle(String titleStr) {
        mTextView.setText(titleStr);
    }

    public void setButtonTextTitle(int titleRes) {
        mTextView.setText(titleRes);
    }

    public void setButtonTextColor(int colorRes) {
        mTextView.setTextColor(colorRes);
    }

    public void setOnButtonCliclListener(OnButtonClickListener listener) {
        this.mListener = listener;
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onButtonClick();
                }
            }
        });
    }

    public interface OnButtonClickListener {
        void onButtonClick();
    }

}
