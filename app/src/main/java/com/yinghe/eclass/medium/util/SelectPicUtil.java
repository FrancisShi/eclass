package com.yinghe.eclass.medium.util;

import android.app.Activity;
import android.content.DialogInterface;

import com.yinghe.eclass.R;

import java.util.ArrayList;
import java.util.List;

public class SelectPicUtil {

    public static void addPic(final Activity activity) {
        List<String> tagActions = new ArrayList<>();
        tagActions.add(activity.getString(R.string.select_from_album));

        DialogUtil.showListDialog(activity, null, tagActions, new DialogInterface.OnClickListener
                () {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (0 == which) {
                    selectFromAlbum(activity);
                }
                dialog.dismiss();
            }
        });

    }

    private static void selectFromAlbum(Activity activity) {
        // TODO: 16/6/30  
    }

}


