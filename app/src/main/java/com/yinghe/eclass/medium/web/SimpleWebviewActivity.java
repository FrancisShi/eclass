package com.yinghe.eclass.medium.web;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.yinghe.eclass.R;
import com.yinghe.eclass.medium.base.AbsBackActivity;

import butterknife.BindView;

public class SimpleWebviewActivity extends AbsBackActivity {

    public static final String WEBVIEW_URL = "WEBVIEW_URL";

    private String mUrl;
    private WebViewClient mWebViewClient;
    private WebChromeClient mWebChromeClient;

    @BindView(R.id.activity_simple_webview)
    SimpleWebView mSimpleWebView;
    @BindView(R.id.activity_simple_progress_bar)
    ProgressBar mProgressBar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUrl = getIntent().getStringExtra(WEBVIEW_URL);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        initClient();
        mSimpleWebView.loadUrl(mUrl);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_simple_webview;
    }

    private void initClient() {
        mWebViewClient = new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mProgressBar.setVisibility(View.GONE);
            }
        };

        mWebChromeClient = new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                mProgressBar.setProgress(newProgress);
            }
        };

        mSimpleWebView.setWebChromeClient(mWebChromeClient);
        mSimpleWebView.setWebViewClient(mWebViewClient);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSimpleWebView = null;
    }
}
