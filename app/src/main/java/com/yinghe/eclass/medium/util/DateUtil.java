package com.yinghe.eclass.medium.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    private static final String DATE_FORMAT_FOR_PIC_PATH = "yyyyMMdd_HHmmss";

    public static String getCurrentTimeFormatToDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_FOR_PIC_PATH);
        return dateFormat.format(new Date(System.currentTimeMillis()));
    }

    public static String getStrTime(long cc_time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH时");
        String re_StrTime = sdf.format(new Date(cc_time));
        return re_StrTime;
    }
}
