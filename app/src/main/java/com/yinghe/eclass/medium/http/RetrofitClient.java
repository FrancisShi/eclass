package com.yinghe.eclass.medium.http;

import com.yinghe.eclass.business.data.UserInfo;
import com.yinghe.eclass.business.entity.ChooseClassroom;
import com.yinghe.eclass.business.entity.ClassStudent;
import com.yinghe.eclass.business.entity.ClassroomGroup;
import com.yinghe.eclass.business.entity.ClassroomPride;
import com.yinghe.eclass.business.entity.ClassroomSelfStatic;
import com.yinghe.eclass.business.entity.CourseCat;
import com.yinghe.eclass.business.entity.CourseClassWork;
import com.yinghe.eclass.business.entity.CourseNote;
import com.yinghe.eclass.business.entity.ExamItem;
import com.yinghe.eclass.business.entity.Homework;
import com.yinghe.eclass.business.entity.HomeworkDetail;
import com.yinghe.eclass.business.entity.UserLogin;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class RetrofitClient {

   // public static final String BASE_URL = "http://121.42.139.218:8188/campus_maven/";
    public static final String BASE_URL = "http://www.hnyoujiao.cn/campus_maven/";

    private static final int DEFAULT_TIMEOUT = 5;

    private Retrofit retrofit;
    private ClassService classService;

    //构造方法私有
    private RetrofitClient() {

        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        httpClientBuilder.connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
        httpClientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                HttpUrl url = request.url().newBuilder()
                        .addQueryParameter("isMobile", "1")
                        .addQueryParameter("__sid", UserInfo.getSessionId())
                        .build();
                request = request.newBuilder().url(url).build();
                return chain.proceed(request);

            }
        });

        retrofit = new Retrofit.Builder()
                .client(httpClientBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(BASE_URL)
                .build();


        classService = retrofit.create(ClassService.class);
    }

    //在访问HttpMethods时创建单例
    private static class SingletonHolder {
        private static final RetrofitClient INSTANCE = new RetrofitClient();
    }

    //获取单例
    public static RetrofitClient getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private class HttpResultFunc<T> implements Func1<HttpResult<T>, T> {

        @Override
        public T call(HttpResult<T> httpResult) {
            if (httpResult.getSuccess()) {
                return httpResult.getData();
            } else {
                throw new ApiException(httpResult.getMessage());
            }
        }
    }

    /**
     * login request
     */
    public void login(Subscriber<UserLogin> subscriber, String name, String password) {
        Observable<UserLogin> observable = classService.login(name, password)
                .map(new HttpResultFunc<UserLogin>());
        toSubscribe(observable, subscriber);
    }

    /**
     * course request
     */
    public void course(Subscriber<List<CourseCat>> subscriber, String id) {
        Observable<List<CourseCat>> observable = classService.course(id)
                .map(new HttpResultFunc<List<CourseCat>>());
        toSubscribe(observable, subscriber);
    }

    public void courseNote(Subscriber<List<CourseNote>> subscriber, String id) {
        Observable<List<CourseNote>> observable = classService.courseNote(id)
                .map(new HttpResultFunc<List<CourseNote>>());
        toSubscribe(observable, subscriber);
    }

    public void courseClass(Subscriber<List<CourseClassWork>> subscriber, String id) {
        Observable<List<CourseClassWork>> observable = classService.courseClass(id)
                .map(new HttpResultFunc<List<CourseClassWork>>());
        toSubscribe(observable, subscriber);
    }

    public void courseExam(Subscriber<List<ExamItem>> subscriber) {
        Observable<List<ExamItem>> observable = classService.courseExamList()
                .map(new HttpResultFunc<List<ExamItem>>());
        toSubscribe(observable, subscriber);
    }

    /**
     * classroom request
     */
    public void classroomPride(Subscriber<List<ClassroomPride>> subscriber, String id) {
        Observable<List<ClassroomPride>> observable = classService.classroomPride(id)
                .map(new HttpResultFunc<List<ClassroomPride>>());
        toSubscribe(observable, subscriber);
    }

    public void classroomSelf(Subscriber<ClassroomSelfStatic> subscriber, String id) {
        Observable<ClassroomSelfStatic> observable = classService.classroomSelf(id)
                .map(new HttpResultFunc<ClassroomSelfStatic>());
        toSubscribe(observable, subscriber);
    }

    public void classroomGroup(Subscriber<List<ClassroomGroup>> subscriber, String teacherId,
                               String classId, String studentId) {
        Observable<List<ClassroomGroup>> observable = classService.classroomGroup(teacherId,
                classId, studentId)
                .map(new HttpResultFunc<List<ClassroomGroup>>());
        toSubscribe(observable, subscriber);
    }

    /**
     * homework request
     */
    public void homeworkList(Subscriber<List<Homework>> subscriber, String userId,
                             String homeworkType) {
        Observable<List<Homework>> observable = classService.homeworkList(userId,
                homeworkType)
                .map(new HttpResultFunc<List<Homework>>());
        toSubscribe(observable, subscriber);
    }

    public void homeworkDetail(Subscriber<HomeworkDetail> subscriber, String userId,
                               String homeworkId) {
        Observable<HomeworkDetail> observable = classService.homeworkDetail(userId, homeworkId)
                .map(new HttpResultFunc<HomeworkDetail>());
        toSubscribe(observable, subscriber);
    }

    public void chooseClassroom(Subscriber<List<ChooseClassroom>> subscriber, String userId) {
        Observable<List<ChooseClassroom>> observable = classService.chooseClassroom(userId)
                .map(new HttpResultFunc<List<ChooseClassroom>>());
        toSubscribe(observable, subscriber);
    }

    public void requestStudent(Subscriber<List<ClassStudent>> subscriber, String classId) {
        Observable<List<ClassStudent>> observable = classService.getStudentList(classId)
                .map(new HttpResultFunc<List<ClassStudent>>());
        toSubscribe(observable, subscriber);
    }

    private <T> void toSubscribe(Observable<T> o, Subscriber<T> s) {
        o.subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s);
    }
}