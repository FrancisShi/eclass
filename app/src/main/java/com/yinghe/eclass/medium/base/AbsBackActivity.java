package com.yinghe.eclass.medium.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.yinghe.eclass.R;

import butterknife.BindView;

public abstract class AbsBackActivity extends BaseActivity {

    @BindView(R.id.activity_abs_back_toolbar)
    protected Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LinearLayout baseLayout = (LinearLayout) getLayoutInflater().inflate(R.layout
                .activity_abs_back, null);
        FrameLayout subActivityContent = (FrameLayout) baseLayout.findViewById(R.id
                .activity_abs_back_content_frame);
        getLayoutInflater().inflate(getLayout(), subActivityContent, true);
        setContentView(baseLayout);
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);

        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_action_back_black);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onActionBack();
            }
        });
    }

    public abstract int getLayout();

    protected void onActionBack() {
        onBackPressed();
    }

    public void setTitle(String titleStr) {
        if (null != getSupportActionBar()) {
            getSupportActionBar().setTitle(titleStr);
        }
    }
}
