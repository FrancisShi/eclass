package com.yinghe.eclass.medium.signal;

import android.app.Service;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.zsoft.SignalA.Hubs.HubConnection;
import com.zsoft.SignalA.Hubs.HubOnDataCallback;
import com.zsoft.SignalA.Hubs.IHubProxy;
import com.zsoft.SignalA.Transport.Longpolling.LongPollingTransport;
import com.zsoft.SignalA.Transport.StateBase;

import org.json.JSONArray;

public class SignalService extends Service {
    //    private final static String HUB_URL = "http://116.251.96" +
//            ".165:8010/api/EClassSignalr/TestSignalr";
    private final static String HUB_URL = "http://116.251.96.165:8010/signalr";

    private IHubProxy hub = null;

    private HubConnection conn = new HubConnection(HUB_URL, this, new LongPollingTransport()) {
        @Override
        public void OnError(Exception exception) {
        }

        @Override
        public void OnMessage(String message) {
        }

        @Override
        public void OnStateChanged(StateBase oldState, StateBase newState) {
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        beginConnect();
        return super.onStartCommand(intent, flags, startId);
    }

    private void beginConnect() {
        try {
            hub = conn.CreateHubProxy("ChatHub");
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
        hub.On("addNewMessageToPage", new HubOnDataCallback() {
            @Override
            public void OnReceived(JSONArray args) {
                Log.d("FUCK", args.toString());
            }
        });
        conn.Start();
    }

    public void startSignalA() {
        if (conn != null)
            conn.Start();
    }

    public void stopSignalA() {
        if (conn != null)
            conn.Stop();
    }

    public void sendMessage(String message) {

    }
}
