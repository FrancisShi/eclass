package com.yinghe.eclass.medium.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.ArrayAdapter;

import com.yinghe.eclass.R;

import java.util.List;


public class DialogUtil {

    private static android.support.v7.app.AlertDialog alertDialog;

    public static void dismissDialog() {
        if (null != alertDialog && alertDialog.isShowing() && null != alertDialog.getWindow()) {
            alertDialog.dismiss();
        }
    }

    public static void showDialog(Context context, String title, CharSequence message, String
            positiveMessage, String negativeMessage,
                                  final OnClickListener positiveClickListener,
                                  final OnClickListener negativeClickListener, boolean cancelable) {
        dismissDialog();
        if (null != alertDialog && alertDialog.isShowing()) {
            return;
        }
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app
                .AlertDialog.Builder(context);
        builder.setCancelable(cancelable)
                .setPositiveButton(positiveMessage, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (null != positiveClickListener) {
                            positiveClickListener.onClick();
                        }
                    }
                })
                .setNegativeButton(negativeMessage, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (null != negativeClickListener) {
                            negativeClickListener.onClick();
                        }
                    }
                })
                .setTitle(title)
                .setMessage(message);
        alertDialog = builder.create();
        setDialogColor(context);
        alertDialog.show();
    }

    public static void showDialog(Context context, String title, String message, int
            positiveMessage, boolean cancelable) {

        showDialog(context, title, message, context.getString(positiveMessage), null, null, null,
                cancelable);

    }

    public static void showDialog(Context context, String message, String positiveMessage, String
            negativeMessage,
                                  final OnClickListener positiveClickListener,
                                  final OnClickListener negativeClickListener, boolean cancelable) {

        showDialog(context, null, message, positiveMessage, negativeMessage,
                positiveClickListener, negativeClickListener, cancelable);

    }

    public static void showDialog(Context context, int message, int positiveMessage,
                                  final OnClickListener positiveClickListener, boolean cancelable) {

        showDialog(context, null, context.getString(message), context.getString(positiveMessage),
                context.getString(R.string.cancel), positiveClickListener, null, cancelable);

    }

    public static void showDialog(Context context, int message, int positiveMessage, boolean
            cancelable) {

        showDialog(context, null, context.getString(message), context.getString(positiveMessage),
                null, null, null, cancelable);
    }

    public static void showDialog(Context context, int title, String message, int
            positiveMessage, final OnClickListener positiveClickListener, boolean cancelable) {
        showDialog(context, context.getString(title), message,
                context.getString(positiveMessage),
                null, positiveClickListener, null, cancelable);
    }

    public static void showDialog(Context context, int title, int message, int positiveMessage,
                                  int negativeMessage,
                                  final OnClickListener positiveClickListener,
                                  final OnClickListener negativeClickListener, boolean cancelable) {

        showDialog(context, context.getString(title), context.getString(message), context
                        .getString(positiveMessage), context.getString(negativeMessage),
                positiveClickListener, negativeClickListener, cancelable);

    }

    public static void showDialog(Context context, int title, String message, int
            positiveMessage, int negativeMessage,
                                  final OnClickListener positiveClickListener,
                                  final OnClickListener negativeClickListener, boolean cancelable) {

        showDialog(context, context.getString(title), message, context.getString(positiveMessage),
                context.getString(negativeMessage),
                positiveClickListener, negativeClickListener, cancelable);
    }

    public static void showDialogNoNegativeButton(Context context, int title, String message, int
            positiveMessage, final OnClickListener positiveClickListener, boolean cancelable) {
        showDialog(context, context.getString(title), message, context.getString(positiveMessage),
                null, positiveClickListener, null, cancelable);
    }

    public static void showListDialog(Context context, String title, List<String> list,
                                        final DialogInterface.OnClickListener onClickListener) {
        dismissDialog();
        if (null != alertDialog && alertDialog.isShowing()) {
            return;
        }
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app
                .AlertDialog.Builder(context);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, R.layout
                .list_material_dialog_item);
        arrayAdapter.addAll(list);
        builder.setTitle(title);
        builder.setAdapter(arrayAdapter, onClickListener);
        alertDialog = builder.create();
        alertDialog.show();
    }

    public interface OnClickListener {
        void onClick();
    }

    /**
     * 修改按钮颜色
     *
     * @param context
     */
    private static void setDialogColor(final Context context) {
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(context
                        .getResources().getColor(R.color.material_dialog_negative));
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(context
                        .getResources().getColor(R.color.material_dialog_positive));
            }
        });

    }

}
