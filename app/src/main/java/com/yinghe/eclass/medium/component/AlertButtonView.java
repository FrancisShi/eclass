package com.yinghe.eclass.medium.component;

import android.content.Context;
import android.util.AttributeSet;

import com.yinghe.eclass.R;

public class AlertButtonView extends ButtonView {
    public AlertButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setButtonTextColor(context.getResources().getColor(R.color.button_view_text_color_alert));
    }
}
