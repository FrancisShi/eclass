package com.yinghe.eclass.medium.util;

import android.content.SharedPreferences;

import com.yinghe.eclass.business.EClassApplication;

public class SharePrefUtil {
    /**
     * 文件持久化地址管理<br/>
     */
    public enum FileName {
        DEFAULT_PREFS("com.yinghe.eclass.PREFS");
        private String mFileName;

        private FileName(String fileName) {
            this.mFileName = fileName;
        }

        public String getFileName() {
            return mFileName;
        }

        public void setFileName(String fileName) {
            this.mFileName = fileName;
        }

    }

    /**
     * 保存数据，默认保存在data/data/shared_prefs/com.yinghe.eclass.PREFS文件夹下
     *
     * @param key
     * @param object
     */
    public static void putData(String key, Object object) {
        put(key, object, FileName.DEFAULT_PREFS);
    }

    /**
     * 保存数据
     *
     * @param key
     * @param object
     * @param fileName 文件名
     */
    public static void putData(String key, Object object, FileName fileName) {
        put(key, object, fileName);
    }

    /**
     * 保存数据
     */
    private static void put(String key, Object object, FileName fileName) {
        String type = object.getClass().getSimpleName();
        SharedPreferences.Editor editor = getSharePrefs(fileName).edit();

        if ("String".equals(type)) {
            editor.putString(key, (String) object);
        } else if ("Integer".equals(type)) {
            editor.putInt(key, (Integer) object);
        } else if ("Boolean".equals(type)) {
            editor.putBoolean(key, (Boolean) object);
        } else if ("Float".equals(type)) {
            editor.putFloat(key, (Float) object);
        } else if ("Long".equals(type)) {
            editor.putLong(key, (Long) object);
        }

        /**
         * 1、apply没有返回值，commit会返回一个Boolean值，表明是否修改成功
         * 2、apply是将修改的数据提交到了内存，而后异步真正提交到硬件磁盘；而commit是同步提交到硬件磁盘，因此在多个并发的提交commit
         * 的时候，它们会等待正在处理的commit保存到磁盘后再操作，从而降低了效率；
         *    而apply只是原子的提交到内容，后面有调用apply的函数，将会直接覆盖前面的内存数据，这样从一定程度上提高了很多效率
         * 3、apply方法不会提示任何失败的提示
         */
        editor.apply();
    }

    public static Object getData(String key, Object defaultObject) {
        return get(key, defaultObject, FileName.DEFAULT_PREFS);
    }

    public static Object getData(String key, Object defaultObject, FileName fileName) {
        return get(key, defaultObject, fileName);
    }

    /**
     * 得到保存数据的方法，我们根据默认值得到保存的数据的具体类型，然后调用相对于的方法获取值
     * getData的时候如果默认值是Long类型，defaultObject一定要按照规范传(后面带L),例如：0L
     *
     * @param key
     * @param defaultObject
     * @return
     */
    private static Object get(String key, Object defaultObject, FileName fileName) {
        String type = defaultObject.getClass().getSimpleName();
        SharedPreferences sp = getSharePrefs(fileName);
        if ("String".equals(type)) {
            return sp.getString(key, (String) defaultObject);
        } else if ("Integer".equals(type)) {
            return sp.getInt(key, (Integer) defaultObject);
        } else if ("Boolean".equals(type)) {
            return sp.getBoolean(key, (Boolean) defaultObject);
        } else if ("Float".equals(type)) {
            return sp.getFloat(key, (Float) defaultObject);
        } else if ("Long".equals(type)) {
            return sp.getLong(key, (Long) defaultObject);
        }

        return null;
    }

    /**
     * remove the key and value
     *
     * @param key
     * @param fileName
     * @return
     */
    public static SharedPreferences.Editor remove(String key, FileName fileName) {
        SharedPreferences.Editor editor = getSharePrefs(fileName).edit().remove(key);
        editor.apply();
        return editor;
    }

    /**
     * Mark in the editor to remove <em>all</em> values from the
     * preferences.
     *
     * @param fileName
     * @return
     */
    public static SharedPreferences.Editor clear(FileName fileName) {
        SharedPreferences.Editor editor = getSharePrefs(fileName).edit().clear();
        editor.apply();
        return editor;
    }

    /**
     * get SharePrefrence instance<br/>
     *
     * @param fileName
     * @return
     */
    public static SharedPreferences getSharePrefs(FileName fileName) {
        if (fileName == null) {
            fileName = FileName.DEFAULT_PREFS;
        }
        return EClassApplication.getInstance().getPrefs(fileName.getFileName());
    }

}

