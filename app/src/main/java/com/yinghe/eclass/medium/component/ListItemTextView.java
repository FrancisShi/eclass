package com.yinghe.eclass.medium.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yinghe.eclass.R;

public class ListItemTextView extends RelativeLayout {

    private Context mContext;
    private TextView mItemTitleView;
    private TextView mItemTextView;
    private ImageView mArrawImage;

    private String mTitle;
    private String mItemText;
    private int mTitleColor;
    private int mTextColor;
    private boolean mHaveArrow;

    private static final String DEFAULT_TITLE_COLOR = "#505050";
    private static final String DEFAULT_TEXT_COLOR = "#999999";

    public ListItemTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.component_list_item_text, this, false);
        mItemTitleView = (TextView) view.findViewById(R.id.component_item_title);
        mItemTextView = (TextView) view.findViewById(R.id.component_item_text);
        mArrawImage = (ImageView) view.findViewById(R.id.component_item_arrow_icon);

        TypedArray typedArray = context.obtainStyledAttributes(attrs,
                R.styleable.ListItemTextView);
        mTitle = typedArray.getString(R.styleable.ListItemTextView_itemTextViewTitle);
        mItemText = typedArray.getString(R.styleable.ListItemTextView_itemTextViewText);
        mTitleColor = typedArray.getColor(R.styleable.ListItemTextView_itemTextViewTitleColor, Color
                .parseColor(DEFAULT_TITLE_COLOR));
        mTextColor = typedArray.getColor(R.styleable.ListItemTextView_itemTextViewTextColor, Color
                .parseColor(DEFAULT_TEXT_COLOR));
        mHaveArrow = typedArray.getBoolean(R.styleable.ListItemTextView_itemTextViewWithArrow,
                false);

        mItemTitleView.setText(mTitle);
        mItemTitleView.setTextColor(mTitleColor);

        mItemTextView.setText(mItemText);
        mItemTextView.setTextColor(mTextColor);

        mArrawImage.setVisibility(mHaveArrow ? View.VISIBLE : View.GONE);

        typedArray.recycle();

        addView(view);
    }

    public void setText(int text) {
        mItemTextView.setText(text);
    }

    public void setText(String text) {
        mItemTextView.setText(text);
    }

    public void setArrawVisible(boolean visible) {
        mArrawImage.setVisibility(visible ? View.VISIBLE : View.GONE);
    }
}
