package com.yinghe.eclass.medium.http;

public interface ProgressCancelListener {
    void onCancelProgress();
}
