package com.yinghe.eclass.medium.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yinghe.eclass.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ListItemImageView extends RelativeLayout {

    private Context mContext;
    private TextView mItemTitleView;
    private CircleImageView mItemImage;
    private ImageView mArrawImage;

    private String mTitle;
    private int mTitleColor;
    private boolean mHaveArrow;

    private static final String DEFAULT_TITLE_COLOR = "#505050";

    public ListItemImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.component_list_item_image, this, false);
        mItemTitleView = (TextView) view.findViewById(R.id.component_item_title);
        mItemImage = (CircleImageView) view.findViewById(R.id.component_item_image);
        mArrawImage = (ImageView) view.findViewById(R.id.component_item_arrow_icon);

        TypedArray typedArray = context.obtainStyledAttributes(attrs,
                R.styleable.ListItemImageView);
        mTitle = typedArray.getString(R.styleable.ListItemImageView_itemImageViewTitle);
        mTitleColor = typedArray.getColor(R.styleable.ListItemImageView_itemImageViewTitleColor,
                Color.parseColor(DEFAULT_TITLE_COLOR));
        Drawable drawable = typedArray.getDrawable(R.styleable
                .ListItemImageView_itemImageViewImage);
        mItemImage.setImageDrawable(drawable);
        mHaveArrow = typedArray.getBoolean(R.styleable.ListItemImageView_itemImageViewWithArrow,
                false);

        mItemTitleView.setText(mTitle);
        mItemTitleView.setTextColor(mTitleColor);
        mArrawImage.setVisibility(mHaveArrow ? View.VISIBLE : View.GONE);

        typedArray.recycle();

        addView(view);
    }

    public void setImage(int res) {
        mItemImage.setImageResource(res);
    }

    public ImageView getImageView() {
        return mItemImage;
    }

    public void setArrawVisible(boolean visible) {
        mArrawImage.setVisibility(visible ? View.VISIBLE : View.GONE);
    }
}
