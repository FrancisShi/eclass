package com.yinghe.eclass.medium.http;

public class HttpResult<T> {
    private boolean success;
    private String message;
    private T data;

    public boolean getSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public T getData() {
        return data;
    }
}