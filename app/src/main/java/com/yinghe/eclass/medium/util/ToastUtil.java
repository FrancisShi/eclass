package com.yinghe.eclass.medium.util;

import android.content.Context;
import android.widget.Toast;

import com.yinghe.eclass.R;

public class ToastUtil {

    private static Toast toast;

    public static void show(Context context, String res) {
        if (null == context) {
            return;
        }

        toast = Toast.makeText(context, res, Toast.LENGTH_SHORT);
        toast.show();
    }

    public static void show(Context context, int res) {
        if(null == context){
            return;
        }

        toast = Toast.makeText(context, res, Toast.LENGTH_SHORT);
        toast.show();
    }

    public static void requestDataFail(Context context) {
        if (null == context) {
            return;
        }

        toast = Toast.makeText(context, R.string.request_data_fail, Toast.LENGTH_SHORT);
        toast.show();
    }
}
