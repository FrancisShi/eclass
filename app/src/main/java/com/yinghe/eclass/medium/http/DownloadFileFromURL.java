package com.yinghe.eclass.medium.http;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.yinghe.eclass.medium.util.FileUtil;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class DownloadFileFromURL extends AsyncTask<String, String, String> {

    /**
     * Downloading file in background thread
     */
    @Override
    protected String doInBackground(String... f_url) {
        int count;
        File file = null;
        try {
            URL url = new URL(f_url[0]);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream
            File savePath = new File(FileUtil.getDownloadFilePath());
            if (!savePath.exists()) {
                savePath.mkdirs();
            }
            file = new File(savePath, fileName);
            OutputStream output = new FileOutputStream(file);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            if (mDownloadCallback != null) {
                mDownloadCallback.callbackError(e.getMessage());
            }
        }

        if (file == null) {
            return null;
        }
        return file.getPath();
    }

    /**
     * Updating progress bar
     */
    protected void onProgressUpdate(String... progress) {
        if (mDownloadCallback != null) {
            mDownloadCallback.callbackUpdate(Integer.parseInt(progress[0]));
        }
    }

    /**
     * After completing background task Dismiss the progress dialog
     **/
    @Override
    protected void onPostExecute(String file_url) {
        if (mDownloadCallback != null) {
            mDownloadCallback.callbackSuccess(file_url);
        }

    }

    private DownloadCallback mDownloadCallback;

    public void setDownloadCallback(DownloadCallback downloadCallback) {
        this.mDownloadCallback = downloadCallback;
    }

    private String fileName;

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public interface DownloadCallback {
        void callbackError(String msg);

        void callbackSuccess(String fileUrl);

        void callbackUpdate(int progress);

    }
}