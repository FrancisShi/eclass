package com.yinghe.eclass.business.ui.student;

import android.os.Bundle;

import com.yinghe.eclass.R;
import com.yinghe.eclass.medium.base.BaseFragment;

/**
 *  * description
 *  *
 *  * @author Francis @Hangzhou Youzan Technology Co.Ltd
 *  * @date 16/6/19
 *  
 */
public class StudentProjectionFragment extends BaseFragment {
    @Override
    public int getContentViewId() {
        return R.layout.fragment_student_projection;
    }

    public static StudentProjectionFragment newInstance() {
        
        Bundle args = new Bundle();
        
        StudentProjectionFragment fragment = new StudentProjectionFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
