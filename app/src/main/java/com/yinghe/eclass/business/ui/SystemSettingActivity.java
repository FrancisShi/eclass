package com.yinghe.eclass.business.ui;

import com.yinghe.eclass.R;
import com.yinghe.eclass.medium.base.AbsBackActivity;

/**
 * @author Francis
 * @date 16/6/24
 */
public class SystemSettingActivity extends AbsBackActivity {
    @Override
    public int getLayout() {
        return R.layout.activity_system_setting;
    }
}
