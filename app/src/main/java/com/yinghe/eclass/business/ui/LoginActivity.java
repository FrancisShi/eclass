package com.yinghe.eclass.business.ui;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.yinghe.eclass.R;
import com.yinghe.eclass.business.data.UserInfo;
import com.yinghe.eclass.business.entity.UserLogin;
import com.yinghe.eclass.medium.base.BaseActivity;
import com.yinghe.eclass.medium.http.ProgressSubscriber;
import com.yinghe.eclass.medium.http.RetrofitClient;
import com.yinghe.eclass.medium.http.SubscriberOnNextListener;
import com.yinghe.eclass.medium.util.DialogUtil;
import com.yinghe.eclass.medium.util.ToastUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity {

    private boolean mIsLogining = false;
    private String userName;
    private String password;
    SubscriberOnNextListener<UserLogin> loginSubscriberOnNextListener;

    @BindView(R.id.activity_login_account_edit_text)
    EditText mLoginEditText;
    @BindView(R.id.activity_login_password_edit_text)
    EditText mPasswordEditText;
    @BindView(R.id.login_version_text)
    TextView mLoginVersionTextView;

    @OnClick(R.id.activity_login_button)
    public void sendLogin() {
        userName = mLoginEditText.getText().toString();
        password = mPasswordEditText.getText().toString();

        if ("".equals(userName) || "".equals(password)) {
            DialogUtil.showDialog(this, R.string.login_account_password_cannot_be_empty, R.string
                    .know, false);
        } else {
            InputMethodManager imm = (InputMethodManager) this.getSystemService(Context
                    .INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mLoginEditText.getWindowToken(), 0);
            //loginSuccessTest();
            // TODO: 16/6/2  
            doLogin();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setTitle(getString(R.string.login));
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getApplicationContext()
                    .getPackageName(), 0);
            mLoginVersionTextView.setText(info.versionName);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void doLogin() {
        loginSubscriberOnNextListener = new SubscriberOnNextListener<UserLogin>() {

            @Override
            public void onNext(UserLogin userLogin) {
                int userType = userLogin.getUsertype();
                if (userType == 2 || userType == 3) {
                    UserInfo.saveNickname(userLogin.getName());
                    UserInfo.saveAvatar(RetrofitClient.BASE_URL.substring(0, RetrofitClient.BASE_URL.length() - 1) + userLogin.getPicurl());
                    UserInfo.saveUserId(userLogin.getUserid());
                    UserInfo.saveUserType(userType);
                    UserInfo.saveStatus(userLogin.getStatus());
                    UserInfo.saveSessionId(userLogin.getMobilesessionid());
                    List<UserLogin.Classlist> classList = userLogin.getClasslist();
                    if (classList != null && classList.size() > 0) {
                        UserInfo.saveClassId(classList.get(0).getId());
                    }
                    UserInfo.saveIsAlreadyLogin(true);

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    ToastUtil.show(LoginActivity.this, "用户类型错误,请选择学生或教师用户");
                }
            }
        };
        RetrofitClient.getInstance().login(new ProgressSubscriber<UserLogin>
                (loginSubscriberOnNextListener, this), userName, password);
    }

    @Override
    public void onBackPressed() {
        DialogUtil.showDialog(this,
                R.string.exit_app_warning, R.string.confirm,
                new DialogUtil.OnClickListener() {
                    @Override
                    public void onClick() {
                        finish();
                    }
                }, false
        );
    }
}