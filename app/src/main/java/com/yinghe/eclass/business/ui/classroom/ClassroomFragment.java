package com.yinghe.eclass.business.ui.classroom;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.yinghe.eclass.R;
import com.yinghe.eclass.business.entity.CourseNote;
import com.yinghe.eclass.medium.base.BaseFragment;
import com.yinghe.eclass.medium.http.SubscriberOnNextListener;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import me.kaelaela.verticalviewpager.transforms.DefaultTransformer;

public class ClassroomFragment extends BaseFragment {

    private static final int PAGER_SIZE = 2;
    private MyPagerAdapter mPagerAdapter;

    @BindView(R.id.activity_classroom_view_pager)
    ViewPager viewPager;

    @OnClick({R.id.classroom_pride, R.id.classroom_group})
    public void tabClicked(View view) {
        int index = 0;
        switch (view.getId()) {
            case R.id.classroom_pride:
                index = 0;
                break;
            case R.id.classroom_group:
                index = 1;
                break;
            default:
                break;
        }
        viewPager.setCurrentItem(index);
    }

    public static ClassroomFragment newInstance() {
        ClassroomFragment fragment = new ClassroomFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getContentViewId() {
        return R.layout.fragment_classroom;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initPager();
    }

    private void initPager() {
        mPagerAdapter = new MyPagerAdapter(getFragmentManager());
        viewPager.setOffscreenPageLimit(PAGER_SIZE);
        viewPager.setAdapter(mPagerAdapter);
        viewPager.setPageTransformer(false, new DefaultTransformer());

        viewPager.setCurrentItem(0);
    }

    private class MyPagerAdapter extends FragmentStatePagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            BaseFragment baseFragment = null;
            switch (position) {
                case 0:
                    baseFragment = ClassroomPrideFragment.newInstance();
                    break;
                case 1:
                    baseFragment = ClassroomGroupFragment.newInstance();
                    break;
                default:
                    break;
            }
            return baseFragment;
        }

        @Override
        public int getCount() {
            return PAGER_SIZE;
        }

    }
}
