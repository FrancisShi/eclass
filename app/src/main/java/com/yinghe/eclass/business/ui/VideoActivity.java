package com.yinghe.eclass.business.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.yinghe.eclass.R;
import com.yinghe.eclass.medium.base.AbsBackActivity;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;

public class VideoActivity extends AbsBackActivity {

    public static final String PATH = "PATH";
    private String mPath;

    @Override
    public int getLayout() {
        return R.layout.activity_video;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPath = getIntent().getStringExtra(PATH);
        initVideo();
    }

    private void initVideo() {
        JCVideoPlayerStandard jcVideoPlayerStandard = (JCVideoPlayerStandard) findViewById(R.id
                .custom_videoplayer_standard);
//        jcVideoPlayerStandard.setUp(mPath);
        // TODO: 16/6/17 test
        jcVideoPlayerStandard.setUp("http://static.irematch.com/test.flv"
                , "test");
//        ImageLoader.getInstance().displayImage("http://p.qpic" +
//                        ".cn/videoyun/0/2449_bfbbfa3cea8f11e5aac3db03cda99974_1/640",
//                jcVideoPlayerStandard.thumbImageView);
    }

    @Override
    protected void onPause() {
        super.onPause();
        JCVideoPlayer.releaseAllVideos();
    }
}
