package com.yinghe.eclass.business.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.yinghe.eclass.business.data.UserInfo;
import com.yinghe.eclass.medium.base.BaseActivity;

public class StartActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (UserInfo.getIsAlreadyLogin()) {
            startActivity(new Intent(this, MainActivity.class));
        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }

        finish();
    }

}
