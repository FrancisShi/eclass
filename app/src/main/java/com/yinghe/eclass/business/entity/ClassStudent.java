package com.yinghe.eclass.business.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author Francis
 * @date 16/6/21
 */
public class ClassStudent {

    private String id;
    @SerializedName("classId")
    private String classid;
    @SerializedName("className")
    private String classname;
    @SerializedName("userName")
    private String username;
    @SerializedName("studentName")
    private String studentname;
    @SerializedName("xdName")
    private String xdname;
    @SerializedName("schoolId")
    private String schoolid;
    @SerializedName("loginName")
    private String loginname;
    @SerializedName("picUrl")
    private String picurl;


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }


    public void setClassid(String classid) {
        this.classid = classid;
    }

    public String getClassid() {
        return classid;
    }


    public void setClassname(String classname) {
        this.classname = classname;
    }

    public String getClassname() {
        return classname;
    }


    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }


    public void setStudentname(String studentname) {
        this.studentname = studentname;
    }

    public String getStudentname() {
        return studentname;
    }


    public void setXdname(String xdname) {
        this.xdname = xdname;
    }

    public String getXdname() {
        return xdname;
    }


    public void setSchoolid(String schoolid) {
        this.schoolid = schoolid;
    }

    public String getSchoolid() {
        return schoolid;
    }


    public void setLoginname(String loginname) {
        this.loginname = loginname;
    }

    public String getLoginname() {
        return loginname;
    }


    public void setPicurl(String picurl) {
        this.picurl = picurl;
    }

    public String getPicurl() {
        return picurl;
    }

}