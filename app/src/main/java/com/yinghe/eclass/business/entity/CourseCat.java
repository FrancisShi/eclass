package com.yinghe.eclass.business.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CourseCat {

    private String xkName;
    private List<Course> courses;


    public void setXkName(String xkName) {
        this.xkName = xkName;
    }

    public String getXkName() {
        return xkName;
    }


    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public List<Course> getCourses() {
        return courses;
    }


    public class Course {

        private String id;
        @SerializedName("xdId")
        private String xdid;
        @SerializedName("xdName")
        private String xdname;
        @SerializedName("xkId")
        private String xkid;
        @SerializedName("xkName")
        private String xkname;
        @SerializedName("bbId")
        private String bbid;
        @SerializedName("bbName")
        private String bbname;
        @SerializedName("njId")
        private String njid;
        @SerializedName("njName")
        private String njname;
        @SerializedName("zjId")
        private String zjid;
        @SerializedName("zjName")
        private String zjname;
        @SerializedName("zsdkId")
        private String zsdkid;
        @SerializedName("zsdkName")
        private String zsdkname;
        @SerializedName("zsdId")
        private String zsdid;
        @SerializedName("zsdName")
        private String zsdname;
        private String name;
        private String pid;
        @SerializedName("parentName")
        private String parentname;
        private String content;
        @SerializedName("contentType")
        private String contenttype;
        @SerializedName("contentSize")
        private String contentsize;
        @SerializedName("contentUploadTime")
        private String contentuploadtime;
        @SerializedName("contentUploadPath")
        private String contentuploadpath;
        private String description;
        @SerializedName("imageUploadPath")
        private String imageuploadpath;
        @SerializedName("resourceType")
        private int resourcetype;
        @SerializedName("createPersionId")
        private String createpersionid;
        @SerializedName("createPersionName")
        private String createpersionname;
        @SerializedName("schoolId")
        private String schoolid;
        @SerializedName("schoolName")
        private String schoolname;
        @SerializedName("classId")
        private String classid;
        @SerializedName("className")
        private String classname;
        @SerializedName("viewCount")
        private String viewcount;
        @SerializedName("studentViewScope")
        private int studentviewscope;
        @SerializedName("teacherViewScope")
        private int teacherviewscope;
        @SerializedName("isShare")
        private int isshare;
        private String copyright;
        @SerializedName("wkCount")
        private String wkcount;
        @SerializedName("zyCount")
        private String zycount;
        @SerializedName("cyCount")
        private String cycount;


        public void setId(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }


        public void setXdid(String xdid) {
            this.xdid = xdid;
        }

        public String getXdid() {
            return xdid;
        }


        public void setXdname(String xdname) {
            this.xdname = xdname;
        }

        public String getXdname() {
            return xdname;
        }


        public void setXkid(String xkid) {
            this.xkid = xkid;
        }

        public String getXkid() {
            return xkid;
        }


        public void setXkname(String xkname) {
            this.xkname = xkname;
        }

        public String getXkname() {
            return xkname;
        }


        public void setBbid(String bbid) {
            this.bbid = bbid;
        }

        public String getBbid() {
            return bbid;
        }


        public void setBbname(String bbname) {
            this.bbname = bbname;
        }

        public String getBbname() {
            return bbname;
        }


        public void setNjid(String njid) {
            this.njid = njid;
        }

        public String getNjid() {
            return njid;
        }


        public void setNjname(String njname) {
            this.njname = njname;
        }

        public String getNjname() {
            return njname;
        }


        public void setZjid(String zjid) {
            this.zjid = zjid;
        }

        public String getZjid() {
            return zjid;
        }


        public void setZjname(String zjname) {
            this.zjname = zjname;
        }

        public String getZjname() {
            return zjname;
        }


        public void setZsdkid(String zsdkid) {
            this.zsdkid = zsdkid;
        }

        public String getZsdkid() {
            return zsdkid;
        }


        public void setZsdkname(String zsdkname) {
            this.zsdkname = zsdkname;
        }

        public String getZsdkname() {
            return zsdkname;
        }


        public void setZsdid(String zsdid) {
            this.zsdid = zsdid;
        }

        public String getZsdid() {
            return zsdid;
        }


        public void setZsdname(String zsdname) {
            this.zsdname = zsdname;
        }

        public String getZsdname() {
            return zsdname;
        }


        public void setName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }


        public void setPid(String pid) {
            this.pid = pid;
        }

        public String getPid() {
            return pid;
        }


        public void setParentname(String parentname) {
            this.parentname = parentname;
        }

        public String getParentname() {
            return parentname;
        }


        public void setContent(String content) {
            this.content = content;
        }

        public String getContent() {
            return content;
        }


        public void setContenttype(String contenttype) {
            this.contenttype = contenttype;
        }

        public String getContenttype() {
            return contenttype;
        }


        public void setContentsize(String contentsize) {
            this.contentsize = contentsize;
        }

        public String getContentsize() {
            return contentsize;
        }


        public void setContentuploadtime(String contentuploadtime) {
            this.contentuploadtime = contentuploadtime;
        }

        public String getContentuploadtime() {
            return contentuploadtime;
        }


        public void setContentuploadpath(String contentuploadpath) {
            this.contentuploadpath = contentuploadpath;
        }

        public String getContentuploadpath() {
            return contentuploadpath;
        }


        public void setDescription(String description) {
            this.description = description;
        }

        public String getDescription() {
            return description;
        }


        public void setImageuploadpath(String imageuploadpath) {
            this.imageuploadpath = imageuploadpath;
        }

        public String getImageuploadpath() {
            return imageuploadpath;
        }


        public void setResourcetype(int resourcetype) {
            this.resourcetype = resourcetype;
        }

        public int getResourcetype() {
            return resourcetype;
        }


        public void setCreatepersionid(String createpersionid) {
            this.createpersionid = createpersionid;
        }

        public String getCreatepersionid() {
            return createpersionid;
        }


        public void setCreatepersionname(String createpersionname) {
            this.createpersionname = createpersionname;
        }

        public String getCreatepersionname() {
            return createpersionname;
        }


        public void setSchoolid(String schoolid) {
            this.schoolid = schoolid;
        }

        public String getSchoolid() {
            return schoolid;
        }


        public void setSchoolname(String schoolname) {
            this.schoolname = schoolname;
        }

        public String getSchoolname() {
            return schoolname;
        }


        public void setClassid(String classid) {
            this.classid = classid;
        }

        public String getClassid() {
            return classid;
        }


        public void setClassname(String classname) {
            this.classname = classname;
        }

        public String getClassname() {
            return classname;
        }


        public void setViewcount(String viewcount) {
            this.viewcount = viewcount;
        }

        public String getViewcount() {
            return viewcount;
        }


        public void setStudentviewscope(int studentviewscope) {
            this.studentviewscope = studentviewscope;
        }

        public int getStudentviewscope() {
            return studentviewscope;
        }


        public void setTeacherviewscope(int teacherviewscope) {
            this.teacherviewscope = teacherviewscope;
        }

        public int getTeacherviewscope() {
            return teacherviewscope;
        }


        public void setIsshare(int isshare) {
            this.isshare = isshare;
        }

        public int getIsshare() {
            return isshare;
        }


        public void setCopyright(String copyright) {
            this.copyright = copyright;
        }

        public String getCopyright() {
            return copyright;
        }


        public void setWkcount(String wkcount) {
            this.wkcount = wkcount;
        }

        public String getWkcount() {
            return wkcount;
        }


        public void setZycount(String zycount) {
            this.zycount = zycount;
        }

        public String getZycount() {
            return zycount;
        }


        public void setCycount(String cycount) {
            this.cycount = cycount;
        }

        public String getCycount() {
            return cycount;
        }

    }

}
