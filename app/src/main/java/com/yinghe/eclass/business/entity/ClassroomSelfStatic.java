package com.yinghe.eclass.business.entity;

import com.google.gson.annotations.SerializedName;

public class ClassroomSelfStatic {

    @SerializedName("todayNum")
    private int todaynum;
    @SerializedName("weekNum")
    private int weeknum;
    private int total;


    public void setTodaynum(int todaynum) {
        this.todaynum = todaynum;
    }

    public int getTodaynum() {
        return todaynum;
    }


    public void setWeeknum(int weeknum) {
        this.weeknum = weeknum;
    }

    public int getWeeknum() {
        return weeknum;
    }


    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotal() {
        return total;
    }

}
