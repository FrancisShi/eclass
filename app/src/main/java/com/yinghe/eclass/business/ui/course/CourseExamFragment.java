package com.yinghe.eclass.business.ui.course;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.yinghe.eclass.R;
import com.yinghe.eclass.business.entity.ExamItem;
import com.yinghe.eclass.medium.base.BaseFragment;
import com.yinghe.eclass.medium.http.ProgressSubscriber;
import com.yinghe.eclass.medium.http.RetrofitClient;
import com.yinghe.eclass.medium.http.SubscriberOnNextListener;
import com.yinghe.eclass.medium.util.ViewHolderUtil;
import com.yinghe.eclass.medium.web.SimpleWebviewActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class CourseExamFragment extends BaseFragment {

    SubscriberOnNextListener<List<ExamItem>> ExamSubscriberOnNextListener;

    private List<ExamItem> examList = new ArrayList<>();
    private ExamListAdapter examListAdapter;

    public static CourseExamFragment newInstance() {

        Bundle args = new Bundle();

        CourseExamFragment fragment = new CourseExamFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.course_exam_list)
    ListView listView;

    @Override
    public int getContentViewId() {
        return R.layout.fragment_course_exam;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requestHomeworkList();

    }

    class ExamListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return examList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mActivity.getLayoutInflater().inflate(R.layout
                        .fragment_course_exam_list_item, parent, false);
            }
            TextView countTextView = ViewHolderUtil.get(convertView, R.id.course_exam_list_item_count);
            countTextView.setText(position + 1 + "");
            TextView titleTextView = ViewHolderUtil.get(convertView, R.id.course_exam_list_item_title);
            titleTextView.setText(examList.get(position).getCyname());
            TextView timeTextView = ViewHolderUtil.get(convertView, R.id.course_exam_list_item_time);
            timeTextView.setText(examList.get(position).getPublishtime());
            TextView submitTextView = ViewHolderUtil.get(convertView, R.id
                    .course_exam_list_item_submit);
            //submitTextView.setText("submit already");
            return convertView;
        }
    }

    private void requestHomeworkList() {
        ExamSubscriberOnNextListener = new SubscriberOnNextListener<List<ExamItem>>() {

            @Override
            public void onNext(List<ExamItem> examItemList) {
                if (examItemList.size() > 0) {
                    examList = examItemList;
                    examListAdapter = new ExamListAdapter();
                    listView.setAdapter(examListAdapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            // TODO: 16/6/4 test
                            Intent intent = new Intent(mActivity, SimpleWebviewActivity.class);
                            intent.putExtra(SimpleWebviewActivity.WEBVIEW_URL, "https://baidu.com");
                            startActivity(intent);
                        }
                    });
                }
            }
        };
        RetrofitClient.getInstance().courseExam(new ProgressSubscriber<List<ExamItem>>
                (ExamSubscriberOnNextListener, mActivity));
    }

}
