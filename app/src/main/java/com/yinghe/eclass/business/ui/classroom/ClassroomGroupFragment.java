package com.yinghe.eclass.business.ui.classroom;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yinghe.eclass.R;
import com.yinghe.eclass.business.entity.ClassroomGroup;
import com.yinghe.eclass.medium.base.BaseFragment;
import com.yinghe.eclass.medium.http.ProgressSubscriber;
import com.yinghe.eclass.medium.http.RetrofitClient;
import com.yinghe.eclass.medium.http.SubscriberOnNextListener;
import com.yinghe.eclass.medium.util.ViewHolderUtil;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;

public class ClassroomGroupFragment extends BaseFragment {

    @BindView(R.id.classroom_group_container)
    LinearLayout mGroupsContainer;

    SubscriberOnNextListener<List<ClassroomGroup>> classroomGroupSubscriberOnNextListener;

    @Override
    public int getContentViewId() {
        return R.layout.fragment_classroom_group;
    }

    public static ClassroomGroupFragment newInstance() {

        Bundle args = new Bundle();

        ClassroomGroupFragment fragment = new ClassroomGroupFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requestGroup();
    }

    class ClassroomPrideGridAdapter extends BaseAdapter {

        private List<ClassroomGroup.Studentlist> mStudentlist;

        public void setStudentlist(List<ClassroomGroup.Studentlist> studentlist) {
            this.mStudentlist = studentlist;
        }

        @Override
        public int getCount() {
            return mStudentlist == null ? 0 : mStudentlist.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mActivity.getLayoutInflater().inflate(R.layout
                        .fragment_classroom_group_lgrid_item, parent, false);
            }

            ImageView profileImage = ViewHolderUtil.get(convertView,R.id
                    .classroom_group_grid_item_profile_image);

            TextView textViewName  = ViewHolderUtil.get(convertView,R.id
                    .classroom_group_grid_item_name);
            textViewName.setText(mStudentlist.get(position).getStudentname());

            return convertView;
        }
    }

    private void requestGroup() {
        classroomGroupSubscriberOnNextListener = new
                SubscriberOnNextListener<List<ClassroomGroup>>() {
                    @Override
                    public void onNext(List<ClassroomGroup> classroomGroupList) {
                        if (classroomGroupList.size() > 0) {
                            setGroupsLinearLayout(classroomGroupList);
                        }
                    }
                };
        // TODO: 16/6/7 add class Id
        RetrofitClient.getInstance().classroomGroup(new ProgressSubscriber<List<ClassroomGroup>>
                (classroomGroupSubscriberOnNextListener, mActivity),
                "3cbff1d39e1a42b4815ba7414bb6fb6c",
                "40f37ff721774f259d74157fc2001c57",
                "50f9c0f3e5a641bf9098631cfce9f970");
    }

    private void setGroupsLinearLayout(List<ClassroomGroup> classroomGroupList){
        mGroupsContainer.removeAllViews();

        for (int i = 0;i < classroomGroupList.size();i++) {
            ClassroomGroup item = classroomGroupList.get(i);

            View groupView =  View.inflate(mActivity,R.layout
                    .fragment_classroom_group_gridview,null);
            TextView textViewName = (TextView) groupView.findViewById(R.id.classroom_group_name);
            textViewName.setText(item.getGroupname());

            GridView gridViewGroup = (GridView) groupView.findViewById(R.id
                    .classroom_group_grid_view);

            List<ClassroomGroup.Studentlist> studentlist = item.getParty().getStudentlist();
            if (studentlist != null && studentlist.size() > 0){
                ClassroomPrideGridAdapter groupGridAdapter = new ClassroomPrideGridAdapter();
                groupGridAdapter.setStudentlist(studentlist);
                gridViewGroup.setAdapter(groupGridAdapter);
            }

            mGroupsContainer.addView(groupView);
        }
    }
}
