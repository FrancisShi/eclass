package com.yinghe.eclass.business.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author Francis
 * @date 16/6/24
 */
public class ExamItem {

    private String id;
    @SerializedName("cyId")
    private String cyid;
    @SerializedName("cyName")
    private String cyname;
    @SerializedName("userId")
    private String userid;
    @SerializedName("classId")
    private String classid;
    @SerializedName("publishTime")
    private String publishtime;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }


    public void setCyid(String cyid) {
        this.cyid = cyid;
    }

    public String getCyid() {
        return cyid;
    }


    public void setCyname(String cyname) {
        this.cyname = cyname;
    }

    public String getCyname() {
        return cyname;
    }


    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUserid() {
        return userid;
    }


    public void setClassid(String classid) {
        this.classid = classid;
    }

    public String getClassid() {
        return classid;
    }


    public void setPublishtime(String publishtime) {
        this.publishtime = publishtime;
    }

    public String getPublishtime() {
        return publishtime;
    }

}