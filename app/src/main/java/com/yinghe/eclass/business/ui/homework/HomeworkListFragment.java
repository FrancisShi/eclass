package com.yinghe.eclass.business.ui.homework;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.yinghe.eclass.R;
import com.yinghe.eclass.business.data.UserInfo;
import com.yinghe.eclass.business.entity.Homework;
import com.yinghe.eclass.medium.base.BaseFragment;
import com.yinghe.eclass.medium.http.ProgressSubscriber;
import com.yinghe.eclass.medium.http.RetrofitClient;
import com.yinghe.eclass.medium.http.SubscriberOnNextListener;
import com.yinghe.eclass.medium.util.DateUtil;
import com.yinghe.eclass.medium.util.ViewHolderUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;

public class HomeworkListFragment extends BaseFragment {

    @BindView(R.id.homework_list)
    ListView listView;

    @BindViews({R.id.homework_time, R.id.homework_state, R.id.homework_teacher})
    List<TextView> cateTextViews;

    @OnClick({R.id.homework_time, R.id.homework_state, R.id.homework_teacher})
    public void cateClicked(View v) {
        setTextViewsSelectState(cateTextViews.indexOf(v));
        String orderBy = "create_time";
        switch (v.getId()) {
            case R.id.homework_time:
                orderBy = "create_time";
                break;
            case R.id.homework_state:
                orderBy = "status";
                break;
            case R.id.homework_teacher:
                orderBy = "user_id";
                break;
        }
        requestHomeworkList(orderBy);
    }

    private SubscriberOnNextListener<List<Homework>> HomeworkListSubscriberOnNextListener;
    private HomeWorkListAdapter homeWorkListAdapter;

    public static HomeworkListFragment newInstance() {
        HomeworkListFragment fragment = new HomeworkListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getContentViewId() {
        return R.layout.fragment_homework_list;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setTextViewsSelectState(0);
        homeWorkListAdapter = new HomeWorkListAdapter();
        listView.setAdapter(homeWorkListAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int
                    position, long id) {
                String homeworkId = view.getContentDescription().toString();

                TextView textViewTitle = (TextView) view.findViewById(R.id.homework_list_item_title);
                String title = textViewTitle.getText().toString();
                Intent intent = new Intent(mActivity, HomeworkDetailActivity.class);
                intent.putExtra("title", title);
                intent.putExtra(HomeworkDetailActivity.HOMEWORK_ID, homeworkId);
                startActivity(intent);

            }
        });
        requestHomeworkList("create_time");
    }

    class HomeWorkListAdapter extends BaseAdapter {

        private List<Homework> homeworkList;

        public void setHomeworkList(List<Homework> homeworkList) {
            this.homeworkList = homeworkList;
        }

        @Override
        public int getCount() {
            return homeworkList == null ? 0 : homeworkList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mActivity.getLayoutInflater().inflate(R.layout
                        .fragment_homework_list_item, parent, false);
            }
            TextView countTextView = ViewHolderUtil.get(convertView, R.id.homework_list_item_count);
            countTextView.setText(position + 1 + "");
            TextView titleTextView = ViewHolderUtil.get(convertView, R.id.homework_list_item_title);
            titleTextView.setText(homeworkList.get(position).getTitle());
            TextView timeTextView = ViewHolderUtil.get(convertView, R.id.homework_list_item_time);

            timeTextView.setText(DateUtil.getStrTime(homeworkList.get(position).getCreatetime()));
            TextView submitTextView = ViewHolderUtil.get(convertView, R.id
                    .homework_list_item_submit);
            submitTextView.setText(homeworkList.get(position).getIssubmit() == 0 ? "未提交" : "已提交");
            convertView.setContentDescription(homeworkList.get(position).getId());
            return convertView;
        }
    }

    private void requestHomeworkList(String order) {
        HomeworkListSubscriberOnNextListener = new
                SubscriberOnNextListener<List<Homework>>() {
                    @Override
                    public void onNext(List<Homework> homeworkList) {
                        if (homeworkList.size() > 0) {
                            homeWorkListAdapter.setHomeworkList(homeworkList);
                            homeWorkListAdapter.notifyDataSetChanged();
                        }
                    }
                };
        // TODO: 16/6/7 add class Id
        RetrofitClient.getInstance().homeworkList(new ProgressSubscriber<List<Homework>>
                (HomeworkListSubscriberOnNextListener, mActivity), UserInfo.getUserId(), order);
    }

    private void setTextViewsSelectState(int index) {
        for (int i = 0; i < cateTextViews.size(); i++) {
            TextView textView = cateTextViews.get(i);
            if (index == i) {
                textView.setTextColor(getResources().getColor(R.color.white));
                textView.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            } else {
                textView.setTextColor(getResources().getColor(R.color.item_text));
                textView.setBackgroundColor(Color.TRANSPARENT);
            }
        }

    }
}
