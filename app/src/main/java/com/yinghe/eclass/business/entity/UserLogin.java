package com.yinghe.eclass.business.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserLogin {

    @SerializedName("userId")
    private String userid;
    @SerializedName("userName")
    private String username;
    private String name;
    @SerializedName("userType")
    private int usertype;
    @SerializedName("objectId")
    private String objectid;
    private int status;
    @SerializedName("schoolId")
    private String schoolid;
    @SerializedName("isSystem")
    private int issystem;
    @SerializedName("picUrl")
    private String picurl;
    @SerializedName("mySign")
    private String mysign;
    @SerializedName("mobileSessionId")
    private String mobilesessionid;
    @SerializedName("classList")
    private List<Classlist> classlist;
    @SerializedName("realPath")
    private String realpath;
    @SerializedName("xqId")
    private String xqid;


    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUserid() {
        return userid;
    }


    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public void setUsertype(int usertype) {
        this.usertype = usertype;
    }

    public int getUsertype() {
        return usertype;
    }


    public void setObjectid(String objectid) {
        this.objectid = objectid;
    }

    public String getObjectid() {
        return objectid;
    }


    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }


    public void setSchoolid(String schoolid) {
        this.schoolid = schoolid;
    }

    public String getSchoolid() {
        return schoolid;
    }


    public void setIssystem(int issystem) {
        this.issystem = issystem;
    }

    public int getIssystem() {
        return issystem;
    }


    public void setPicurl(String picurl) {
        this.picurl = picurl;
    }

    public String getPicurl() {
        return picurl;
    }


    public void setMysign(String mysign) {
        this.mysign = mysign;
    }

    public String getMysign() {
        return mysign;
    }


    public void setMobilesessionid(String mobilesessionid) {
        this.mobilesessionid = mobilesessionid;
    }

    public String getMobilesessionid() {
        return mobilesessionid;
    }


    public void setClasslist(List<Classlist> classlist) {
        this.classlist = classlist;
    }

    public List<Classlist> getClasslist() {
        return classlist;
    }


    public void setRealpath(String realpath) {
        this.realpath = realpath;
    }

    public String getRealpath() {
        return realpath;
    }


    public void setXqid(String xqid) {
        this.xqid = xqid;
    }

    public String getXqid() {
        return xqid;
    }


    public class Classlist {

        private String id;
        private String name;
        @SerializedName("shortName")
        private String shortname;
        @SerializedName("classNum")
        private String classnum;
        private int nxnd;
        @SerializedName("schoolId")
        private String schoolid;
        private String ssxd;
        @SerializedName("xdName")
        private String xdname;
        private String state;
        @SerializedName("teacherCourse")
        private String teachercourse;


        public void setId(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }


        public void setName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }


        public void setShortname(String shortname) {
            this.shortname = shortname;
        }

        public String getShortname() {
            return shortname;
        }


        public void setClassnum(String classnum) {
            this.classnum = classnum;
        }

        public String getClassnum() {
            return classnum;
        }


        public void setNxnd(int nxnd) {
            this.nxnd = nxnd;
        }

        public int getNxnd() {
            return nxnd;
        }


        public void setSchoolid(String schoolid) {
            this.schoolid = schoolid;
        }

        public String getSchoolid() {
            return schoolid;
        }


        public void setSsxd(String ssxd) {
            this.ssxd = ssxd;
        }

        public String getSsxd() {
            return ssxd;
        }


        public void setXdname(String xdname) {
            this.xdname = xdname;
        }

        public String getXdname() {
            return xdname;
        }


        public void setState(String state) {
            this.state = state;
        }

        public String getState() {
            return state;
        }


        public void setTeachercourse(String teachercourse) {
            this.teachercourse = teachercourse;
        }

        public String getTeachercourse() {
            return teachercourse;
        }

    }

}

