package com.yinghe.eclass.business.entity;

import com.google.gson.annotations.SerializedName;

public class ChooseClassroom {

    private String id;
    private String name;
    @SerializedName("shortName")
    private String shortname;
    @SerializedName("classNum")
    private String classnum;
    private int nxnd;
    @SerializedName("schoolId")
    private String schoolid;
    private String ssxd;
    @SerializedName("xdName")
    private String xdname;
    private String state;
    @SerializedName("teacherCourse")
    private String teachercourse;


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public String getShortname() {
        return shortname;
    }


    public void setClassnum(String classnum) {
        this.classnum = classnum;
    }

    public String getClassnum() {
        return classnum;
    }


    public void setNxnd(int nxnd) {
        this.nxnd = nxnd;
    }

    public int getNxnd() {
        return nxnd;
    }


    public void setSchoolid(String schoolid) {
        this.schoolid = schoolid;
    }

    public String getSchoolid() {
        return schoolid;
    }


    public void setSsxd(String ssxd) {
        this.ssxd = ssxd;
    }

    public String getSsxd() {
        return ssxd;
    }


    public void setXdname(String xdname) {
        this.xdname = xdname;
    }

    public String getXdname() {
        return xdname;
    }


    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }


    public void setTeachercourse(String teachercourse) {
        this.teachercourse = teachercourse;
    }

    public String getTeachercourse() {
        return teachercourse;
    }

}
