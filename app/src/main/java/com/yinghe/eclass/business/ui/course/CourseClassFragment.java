package com.yinghe.eclass.business.ui.course;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.yinghe.eclass.R;
import com.yinghe.eclass.business.data.UserInfo;
import com.yinghe.eclass.business.entity.CourseClassWork;
import com.yinghe.eclass.medium.base.BaseFragment;
import com.yinghe.eclass.medium.http.ProgressSubscriber;
import com.yinghe.eclass.medium.http.RetrofitClient;
import com.yinghe.eclass.medium.http.SubscriberOnNextListener;
import com.yinghe.eclass.medium.util.DateUtil;
import com.yinghe.eclass.medium.util.ViewHolderUtil;

import java.util.List;

import butterknife.BindView;

public class CourseClassFragment extends BaseFragment {

    SubscriberOnNextListener<List<CourseClassWork>> courseClassSubscriberOnNextListener;

    public static CourseClassFragment newInstance() {

        Bundle args = new Bundle();

        CourseClassFragment fragment = new CourseClassFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getContentViewId() {
        return R.layout.fragment_course_class;
    }

    private ClassListAdapter classListAdapter;

    @BindView(R.id.course_class_list)
    ListView listView;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requestHomeworkList();
    }

    class ClassListAdapter extends BaseAdapter {

        private List<CourseClassWork> courseClassWorkList;

        public void setCourseClassWorkList(List<CourseClassWork> courseClassWorkList) {
            this.courseClassWorkList = courseClassWorkList;
        }

        @Override
        public int getCount() {
            return courseClassWorkList == null ? 0 : courseClassWorkList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mActivity.getLayoutInflater().inflate(R.layout
                        .fragment_course_class_list_item, parent, false);
            }
            TextView countTextView = ViewHolderUtil.get(convertView, R.id
                    .course_class_list_item_count);
            countTextView.setText(position + 1 + "");
            TextView titleTextView = ViewHolderUtil.get(convertView, R.id
                    .course_class_list_item_title);
            titleTextView.setText(courseClassWorkList.get(position).getTitle());
            TextView timeTextView = ViewHolderUtil.get(convertView, R.id
                    .course_class_list_item_time);
            timeTextView.setText(DateUtil.getStrTime(courseClassWorkList.get(position)
                    .getCreatetime()));
            TextView submitTextView = ViewHolderUtil.get(convertView, R.id
                    .course_class_list_item_submit);
            submitTextView.setText(courseClassWorkList.get(position).getIssubmit() == 0 ? "未提交" :
                    "已提交");
            return convertView;
        }
    }

    private void requestHomeworkList() {
        courseClassSubscriberOnNextListener = new SubscriberOnNextListener<List<CourseClassWork>>
                () {

            @Override
            public void onNext(List<CourseClassWork> courseClassWorkList) {
                if (courseClassWorkList.size() > 0) {
                    setClassWorkList(courseClassWorkList);
                }
            }
        };
        RetrofitClient.getInstance().courseClass(new ProgressSubscriber<List<CourseClassWork>>
                (courseClassSubscriberOnNextListener, mActivity), UserInfo.getUserId());
    }

    private void setClassWorkList(final List<CourseClassWork> courseClassWorkList) {
        classListAdapter = new ClassListAdapter();
        classListAdapter.setCourseClassWorkList(courseClassWorkList);
        listView.setAdapter(classListAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textViewTitle = (TextView) view.findViewById(R.id.course_class_list_item_title);
                String title = textViewTitle.getText().toString();
                Intent intent = new Intent(mActivity, CourseClassDetailActivity.class);
                intent.putExtra("item",courseClassWorkList.get(position));
                intent.putExtra("title",title);
                startActivity(intent);
            }
        });
    }
}
