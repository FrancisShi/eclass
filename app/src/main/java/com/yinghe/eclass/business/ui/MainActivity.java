package com.yinghe.eclass.business.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.yinghe.eclass.R;
import com.yinghe.eclass.business.data.UserInfo;
import com.yinghe.eclass.business.ui.classroom.ClassroomFragment;
import com.yinghe.eclass.business.ui.course.CourseFragment;
import com.yinghe.eclass.business.ui.homework.HomeworkListFragment;
import com.yinghe.eclass.business.ui.student.StudentClassFragment;
import com.yinghe.eclass.business.ui.student.StudentFragment;
import com.yinghe.eclass.medium.base.BaseActivity;
import com.yinghe.eclass.medium.util.DialogUtil;
import com.yinghe.whiteboardlib.fragment.WhiteBoardFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;
import me.kaelaela.verticalviewpager.transforms.DefaultTransformer;

public class MainActivity extends BaseActivity {

    private MyPagerAdapter mPagerAdapter;

    private static final int PAGER_SIZE = 4;

    @BindViews({R.id.course_icon, R.id.classroom_icon, R.id.homework_icon, R.id.board_icon})
    List<ImageView> icons;

    @BindView(R.id.course)
    LinearLayout courseLinearLayout;

    @BindView(R.id.user_name)
    TextView userNameTextView;

    @BindView(R.id.user_icon)
    ImageView userImageView;

    @BindView(R.id.classroom_text_name)
    TextView classStudentName;

    @BindView(R.id.activity_main_view_pager)
    ViewPager viewPager;

    @OnClick(R.id.user)
    public void toUser(View view) {
        startActivity(new Intent(this, UserActivity.class));
    }

    @OnClick({R.id.course, R.id.classroom, R.id.homework, R.id.board})
    public void changeFragment(View view) {
        int index = 0;
        switch (view.getId()) {
            case R.id.course:
                index = 0;
                break;
            case R.id.classroom:
                Intent intent = new Intent(StudentClassFragment.STUDENT_TAB_CLICK);
                sendBroadcast(intent);
                index = 1;
                break;
            case R.id.homework:
                index = 2;
                break;
            case R.id.board:
                index = 3;
                break;
            default:
                break;
        }
        viewPager.setCurrentItem(index);
        refreshBarStatus(index);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkUserType();
        changeFragment(courseLinearLayout);
        //checkVersion();
        initPager();
        userNameTextView.setText(UserInfo.getNickname());
        Picasso.with(this)
                .load(UserInfo.getAvatar() + "?__sid=" + UserInfo.getSessionId())
                .error(getResources().getDrawable(R.drawable.person))
                .into(userImageView);
    }

    private void checkUserType() {
        int userType = UserInfo.getUserType();
        if (userType == 2) {
            //教师
            classStudentName.setText("学生");
        } else {
            classStudentName.setText("班级");
        }
    }

    private void initPager() {
        mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        viewPager.setOffscreenPageLimit(PAGER_SIZE);
        viewPager.setAdapter(mPagerAdapter);
        viewPager.setPageTransformer(false, new DefaultTransformer());

        viewPager.setCurrentItem(0);
        refreshBarStatus(0);
    }

    private void checkVersion() {
        // TODO: 16/6/2 callback
    }

    private void refreshBarStatus(int index) {
        icons.get(0).setImageResource(R.drawable.book);
        icons.get(1).setImageResource(R.drawable.student);
        icons.get(2).setImageResource(R.drawable.homework);
        icons.get(3).setImageResource(R.drawable.board);

        switch (index) {
            case 0:
                icons.get(0).setImageResource(R.drawable.book_select);
                break;
            case 1:
                icons.get(1).setImageResource(R.drawable.student_select);
                break;
            case 2:
                icons.get(2).setImageResource(R.drawable.homework_select);
                break;
            case 3:
                icons.get(3).setImageResource(R.drawable.board_select);
                break;
            default:
                break;
        }

    }

    @Override
    public void onBackPressed() {
        DialogUtil.showDialog(this,
                R.string.exit_app_warning, R.string.confirm,
                new DialogUtil.OnClickListener() {
                    @Override
                    public void onClick() {
                        finish();
                    }
                }, false
        );
    }

    private class MyPagerAdapter extends FragmentStatePagerAdapter {


        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = CourseFragment.newInstance();
                    break;
                case 1:
                    if (UserInfo.getUserType() == 2) {
                        fragment = StudentFragment.newInstance();
                    } else {
                        fragment = ClassroomFragment.newInstance();
                    }
                    break;
                case 2:
                    fragment = HomeworkListFragment.newInstance();
                    break;
                case 3:
                    fragment = WhiteBoardFragment.newInstance();
                    break;
                default:
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return PAGER_SIZE;
        }

    }

}
