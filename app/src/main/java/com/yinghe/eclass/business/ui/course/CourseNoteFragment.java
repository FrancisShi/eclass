package com.yinghe.eclass.business.ui.course;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dalong.imageview.ProgressImageView;
import com.yinghe.eclass.R;
import com.yinghe.eclass.business.entity.CourseNote;
import com.yinghe.eclass.business.ui.VideoActivity;
import com.yinghe.eclass.medium.base.BaseFragment;
import com.yinghe.eclass.medium.http.DownloadFileFromURL;
import com.yinghe.eclass.medium.http.ProgressSubscriber;
import com.yinghe.eclass.medium.http.RetrofitClient;
import com.yinghe.eclass.medium.http.SubscriberOnNextListener;
import com.yinghe.eclass.medium.util.DialogUtil;
import com.yinghe.eclass.medium.util.FileUtil;
import com.yinghe.eclass.medium.util.ToastUtil;
import com.yinghe.eclass.medium.util.ViewHolderUtil;

import java.io.File;
import java.util.List;

import butterknife.BindView;

public class CourseNoteFragment extends BaseFragment {

    private String courseId;

    private boolean isDebug = true;
    private static final String TEST_URL = "https://www.irematch.com/file/docx.docx";

    SubscriberOnNextListener<List<CourseNote>> courseNoteSubscriberOnNextListener;

    @BindView(R.id.fragment_course_note_container)
    LinearLayout courseNoteContainer;

    @Override
    public int getContentViewId() {
        return R.layout.fragment_course_note;
    }

    public static CourseNoteFragment newInstance(String courseId) {

        Bundle args = new Bundle();
        args.putString(CourseDetailActivity.COURSE_ID, courseId);
        CourseNoteFragment fragment = new CourseNoteFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        courseId = bundle.getString(CourseDetailActivity.COURSE_ID);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requestFileAndMedia();
    }

    // TODO: 16/6/2  try to avoid this method, try to use universal intent
    public void openExcel() {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(android.content.Intent.ACTION_VIEW);
        intent.setClassName("cn.wps.moffice_eng", "cn.wps.moffice.documentmanager" +
                ".PreStartActivity");
        Uri uri = Uri.fromFile(new File("/mnt/sdcard/test.xls"));
        intent.setData(uri);
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static Intent getWordFileIntent(String filePath) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = Uri.fromFile(new File(filePath));
        intent.setDataAndType(uri, "application/msword");
        return intent;
    }

    private Intent getPdfIntent(String filePath) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = Uri.fromFile(new File(filePath));
        intent.setDataAndType(uri, "application/pdf");
        return intent;
    }

    private Intent getExcelIntent() {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = Uri.fromFile(new File("/mnt/sdcard/test.xls"));
        intent.setDataAndType(uri, "application/vnd.ms-excel");
        return intent;
    }

    private String getFileNameFromUrl(String url) {
        return url.contains("/") ? url.split("/")[url.split("/").length - 1] : url;
    }

    private void clickItem(String type, CourseNote.Kjlist noteItem, ProgressImageView imageView) {

        if (type.equals("1")) {
            if (FileUtil.isWpsInstalled(mActivity.getApplicationContext())) {

                String downloadUrl = RetrofitClient.BASE_URL + getFileNameFromUrl(noteItem
                        .getContentuploadpath());
                if (isDebug) {
                    downloadUrl = TEST_URL;
                }
                String fileName = getFileNameFromUrl(downloadUrl);
                File file = new File(FileUtil.getDownloadFilePath(), fileName);
                if (file.exists() && file.isFile()) {
                    openFile(file.getPath());
                } else {
                    download(imageView, downloadUrl);
                }

            } else {
                DialogUtil.showDialog(mActivity, "尚未安装WPS,是否下载?", "确定", "取消", new
                        DialogUtil.OnClickListener() {
                            // TODO: 16/6/30 update APP
                            @Override
                            public void onClick() {
                            }
                        }, new DialogUtil.OnClickListener() {
                    @Override
                    public void onClick() {

                    }
                }, true);
            }
        } else if (type.equals("2")) {
            String downloadUrl = RetrofitClient.BASE_URL + getFileNameFromUrl(noteItem
                    .getContentuploadpath());
            if (isDebug) {
                downloadUrl = " http://2449.vod.myqcloud" +
                        ".com/2449_bfbbfa3cea8f11e5aac3db03cda99974" +
                        ".f20.mp4";
            }
            String fileName = getFileNameFromUrl(downloadUrl);
            File file = new File(FileUtil.getDownloadFilePath(), fileName);
            if (file.isFile()) {
                openFile(file.getPath());
            } else {
                download(imageView, downloadUrl);
            }
        }
    }

    private void download(final ProgressImageView imageView, final String url) {
        DownloadFileFromURL downloadFileFromURL = new DownloadFileFromURL();
        downloadFileFromURL.setFileName(getFileNameFromUrl(url));
        downloadFileFromURL.setDownloadCallback(new DownloadFileFromURL.DownloadCallback() {
            @Override
            public void callbackError(String msg) {
                ToastUtil.show(mActivity, msg);
            }

            @Override
            public void callbackSuccess(String filePath) {
                if (filePath != null && new File(filePath).isFile()) {
                    openFile(filePath);
                }
            }

            @Override
            public void callbackUpdate(int progress) {
                imageView.setProgress(progress);
            }
        });
        downloadFileFromURL.execute(url);
    }

    private void openFile(String filePath) {
        if (filePath.endsWith(".doc") || filePath.endsWith(".docx")) {
            Intent intent = getWordFileIntent(filePath);
            startActivity(intent);

        } else if (filePath.endsWith(".pdf")) {
            Intent intent = getPdfIntent(filePath);
            startActivity(intent);
        } else if (filePath.endsWith((".mp4"))) {
            playVideo(filePath);
        }
    }

    private void playVideo(String path) {
        Intent intent = new Intent(mActivity, VideoActivity.class);
        intent.putExtra(VideoActivity.PATH, path);
        startActivity(intent);
    }

    private void requestFileAndMedia() {
        courseNoteSubscriberOnNextListener = new SubscriberOnNextListener<List<CourseNote>>() {

            @Override
            public void onNext(List<CourseNote> courseNoteList) {
                if (courseNoteList.size() > 0) {
                    setNoteData(courseNoteList);
                }
            }
        };
        RetrofitClient.getInstance().courseNote(new ProgressSubscriber<List<CourseNote>>
                (courseNoteSubscriberOnNextListener, mActivity), courseId);
    }

    private void setNoteData(List<CourseNote> courseNoteList) {
        courseNoteContainer.removeAllViews();

        for (int i = 0; i < courseNoteList.size(); i++) {

            View noteItemView = View.inflate(mActivity, R.layout.fragment_course_note_item,
                    null);
            TextView textView = (TextView) noteItemView.findViewById(R.id
                    .fragment_course_note_item_name);

            GridView gridView = (GridView) noteItemView.findViewById(R.id
                    .fragment_course_note_item_grid_view);

            CourseNote courseNoteItem = courseNoteList.get(i);
            String type = null;
            if (courseNoteItem.getKjtype().equals("1")) {
                type = "文档";
            } else if (courseNoteItem.getKjtype().equals("2")) {
                type = "多媒体";
            }
            if (type != null) {
                textView.setText(type);

                CourseNoteGridAdapter courseNoteAdapter = new CourseNoteGridAdapter();
                courseNoteAdapter.setNoteList(courseNoteItem.getKjlist());
                courseNoteAdapter.setType(courseNoteItem.getKjtype());
                gridView.setAdapter(courseNoteAdapter);

                courseNoteContainer.addView(noteItemView);
            }
        }
    }

    class CourseNoteGridAdapter extends BaseAdapter {

        private List<CourseNote.Kjlist> noteList;
        private String type;

        public void setType(String type) {
            this.type = type;
        }

        public void setNoteList(List<CourseNote.Kjlist> noteList) {
            this.noteList = noteList;
        }

        @Override
        public int getCount() {
            return noteList == null ? 0 : noteList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mActivity.getLayoutInflater().inflate(R.layout
                        .fragment_course_note_grid_item, parent, false);
            }
            final ProgressImageView imageView = ViewHolderUtil.get(convertView, R.id
                    .course_note_gird_item_image);
            final CourseNote.Kjlist noteItem = noteList.get(position);
            String downloadPath = noteItem.getContentuploadpath();
            if (type.equals("1")) {
                // 文档
                if (downloadPath.endsWith(".doc") || downloadPath.endsWith(".docx")) {
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.elements_doc));
                } else if (downloadPath.endsWith(".pdf")) {
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.elements_pdf));
                }
            } else if (type.equals("2")) {
                // 多媒体
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.elements_video));
            }

            String downloadUrl = RetrofitClient.BASE_URL + getFileNameFromUrl(noteItem
                    .getContentuploadpath());
            if (isDebug && downloadUrl.endsWith(".docx")) {
                downloadUrl = TEST_URL;
            }
            String fileName = getFileNameFromUrl(downloadUrl);
            File file = new File(FileUtil.getDownloadFilePath(), fileName);
            if (file.exists() && file.isFile()) {
                imageView.setProgress(100);
            } else {
                imageView.setProgress(0);
            }

            TextView textView = ViewHolderUtil.get(convertView, R.id
                    .course_note_gird_item_name);
            textView.setText(noteList.get(position).getName());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickItem(type, noteItem, imageView);
                }
            });

            return convertView;
        }
    }
}
