package com.yinghe.eclass.business.entity;

import com.google.gson.annotations.SerializedName;

public class ClassroomPride {

    private String id;
    @SerializedName("studentId")
    private String studentid;
    @SerializedName("studentName")
    private String studentname;
    @SerializedName("praiseNum")
    private int praisenum;
    private int rank;
    @SerializedName("classId")
    private String classid;


    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }


    public void setStudentid(String studentid) {
        this.studentid = studentid;
    }
    public String getStudentid() {
        return studentid;
    }


    public void setStudentname(String studentname) {
        this.studentname = studentname;
    }
    public String getStudentname() {
        return studentname;
    }


    public void setPraisenum(int praisenum) {
        this.praisenum = praisenum;
    }
    public int getPraisenum() {
        return praisenum;
    }


    public void setRank(int rank) {
        this.rank = rank;
    }
    public int getRank() {
        return rank;
    }


    public void setClassid(String classid) {
        this.classid = classid;
    }
    public String getClassid() {
        return classid;
    }

}
