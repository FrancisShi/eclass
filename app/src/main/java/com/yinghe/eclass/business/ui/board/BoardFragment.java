package com.yinghe.eclass.business.ui.board;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.yinghe.eclass.R;
import com.yinghe.eclass.medium.base.BaseFragment;
import com.yinghe.eclass.medium.signal.SignalService;

import butterknife.OnClick;

public class BoardFragment extends BaseFragment {

    @OnClick(R.id.test_signal_conn)
    public void testConn(){
        Intent intent = new Intent(mActivity, SignalService.class);
        mActivity.startService(intent);
    }

    public static BoardFragment newInstance() {
        BoardFragment fragment = new BoardFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getContentViewId() {
        return R.layout.fragment_board;
    }

}
