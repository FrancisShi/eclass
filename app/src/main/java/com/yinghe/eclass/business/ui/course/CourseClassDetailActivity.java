package com.yinghe.eclass.business.ui.course;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.yinghe.eclass.R;
import com.yinghe.eclass.business.entity.CourseClassWork;
import com.yinghe.eclass.medium.base.AbsBackActivity;

import butterknife.BindView;

public class CourseClassDetailActivity extends AbsBackActivity {

    private CourseClassWork mItem;

    @BindView(R.id.homework_content)
    TextView mHomeworkContent;

    @BindView(R.id.homework_pic_title)
    TextView mPicTitleTextView;

    @BindView(R.id.activity_course_class_detail_loogk_answer)
    Button mSubmitButton;

    @Override
    public int getLayout() {
        return R.layout.activity_course_class_detail;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(getIntent().getStringExtra("title"));
        mItem = getIntent().getParcelableExtra("item");

        initData();
    }

    private void initData(){
        String content = mItem.getContent();
        mHomeworkContent.setText(Html.fromHtml(content));

        // TODO: 16/6/17 add pic
        mPicTitleTextView.setVisibility(View.GONE);

        if (mItem.getIssubmit() == 0){
            mSubmitButton.setText("上传答案");
        }
    }
}
