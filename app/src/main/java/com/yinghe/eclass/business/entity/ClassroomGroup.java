package com.yinghe.eclass.business.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ClassroomGroup {

    private String id;
    @SerializedName("groupName")
    private String groupname;
    @SerializedName("classId")
    private String classid;
    @SerializedName("partyCounts")
    private String partycounts;
    @SerializedName("isDefault")
    private int isdefault;
    @SerializedName("groupType")
    private int grouptype;
    @SerializedName("createTime")
    private long createtime;
    @SerializedName("teacherId")
    private String teacherid;
    private Party party;


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }


    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getGroupname() {
        return groupname;
    }


    public void setClassid(String classid) {
        this.classid = classid;
    }

    public String getClassid() {
        return classid;
    }


    public void setPartycounts(String partycounts) {
        this.partycounts = partycounts;
    }

    public String getPartycounts() {
        return partycounts;
    }


    public void setIsdefault(int isdefault) {
        this.isdefault = isdefault;
    }

    public int getIsdefault() {
        return isdefault;
    }


    public void setGrouptype(int grouptype) {
        this.grouptype = grouptype;
    }

    public int getGrouptype() {
        return grouptype;
    }


    public void setCreatetime(int createtime) {
        this.createtime = createtime;
    }

    public long getCreatetime() {
        return createtime;
    }


    public void setTeacherid(String teacherid) {
        this.teacherid = teacherid;
    }

    public String getTeacherid() {
        return teacherid;
    }


    public void setParty(Party party) {
        this.party = party;
    }

    public Party getParty() {
        return party;
    }


    public class Studentlist {

        private String id;
        @SerializedName("groupId")
        private String groupid;
        @SerializedName("partyId")
        private String partyid;
        @SerializedName("userId")
        private String userid;
        @SerializedName("partyName")
        private String partyname;
        @SerializedName("teacherId")
        private String teacherid;
        @SerializedName("studentName")
        private String studentname;


        public void setId(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }


        public void setGroupid(String groupid) {
            this.groupid = groupid;
        }

        public String getGroupid() {
            return groupid;
        }


        public void setPartyid(String partyid) {
            this.partyid = partyid;
        }

        public String getPartyid() {
            return partyid;
        }


        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getUserid() {
            return userid;
        }


        public void setPartyname(String partyname) {
            this.partyname = partyname;
        }

        public String getPartyname() {
            return partyname;
        }


        public void setTeacherid(String teacherid) {
            this.teacherid = teacherid;
        }

        public String getTeacherid() {
            return teacherid;
        }


        public void setStudentname(String studentname) {
            this.studentname = studentname;
        }

        public String getStudentname() {
            return studentname;
        }

    }

    public class Party {

        private String id;
        @SerializedName("partyName")
        private String partyname;
        @SerializedName("personCounts")
        private String personcounts;
        @SerializedName("groupId")
        private String groupid;
        @SerializedName("teacherId")
        private String teacherid;
        @SerializedName("studentList")
        private List<Studentlist> studentlist;

        public void setId(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }


        public void setPartyname(String partyname) {
            this.partyname = partyname;
        }

        public String getPartyname() {
            return partyname;
        }


        public void setPersoncounts(String personcounts) {
            this.personcounts = personcounts;
        }

        public String getPersoncounts() {
            return personcounts;
        }


        public void setGroupid(String groupid) {
            this.groupid = groupid;
        }

        public String getGroupid() {
            return groupid;
        }


        public void setTeacherid(String teacherid) {
            this.teacherid = teacherid;
        }

        public String getTeacherid() {
            return teacherid;
        }


        public void setStudentlist(List<Studentlist> studentlist) {
            this.studentlist = studentlist;
        }

        public List<Studentlist> getStudentlist() {
            return studentlist;
        }

    }
}
