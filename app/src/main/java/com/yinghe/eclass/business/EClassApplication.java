package com.yinghe.eclass.business;

import android.content.SharedPreferences;
import android.support.multidex.MultiDexApplication;

public class EClassApplication extends MultiDexApplication {

    private static EClassApplication mApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        mApplication = EClassApplication.this;

    }

    public static EClassApplication getInstance() {
        return mApplication;
    }

    public SharedPreferences getPrefs(String fileName) {
        return getSharedPreferences(fileName, MODE_PRIVATE);
    }
}

