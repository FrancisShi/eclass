package com.yinghe.eclass.business.data;

import android.content.SharedPreferences;

import com.yinghe.eclass.medium.util.SharePrefUtil;

public class UserInfo {

    private static String NICKNAME = "";
    private static String USERID = "";
    private static String AVATAR = "";
    private static int STATUS = -1;
    private static int USER_TYPE = -1;
    private static String CLASSID = "";
    private static String SESSIONID = "";
    private static boolean IS_ALREADY_LOGIN = false;

    public static class PrefsKey {
        public static final String NICKNAME = "UserInfo" + ".NICKNAME";
        public static final String USERID = "UserInfo" + ".USERID";
        public static final String STATUS = "UserInfo" + ".STATUS";
        public static final String USER_TYPE = "UserInfo" + ".USER_TYPE";
        public static final String AVATAR = "UserInfo" + ".AVATAR";
        public static final String SESSIONID = "UserInfo" + ".SESSIONID";
        public static final String CLASSID = "UserInfo" + ".CLASSID";
        public static final String IS_ALREADY_LOGIN = "UserInfo" + ".IS_ALREADY_LOGIN";
    }

    private static SharedPreferences prefs = SharePrefUtil.getSharePrefs(SharePrefUtil.FileName
            .DEFAULT_PREFS);


    public static void saveNickname(String nickname) {
        SharePrefUtil.putData(PrefsKey.NICKNAME, nickname, SharePrefUtil.FileName.DEFAULT_PREFS);
    }

    public static void saveSessionId(String sessionId) {
        SharePrefUtil.putData(PrefsKey.SESSIONID, sessionId, SharePrefUtil.FileName.DEFAULT_PREFS);
    }

    public static void saveUserId(String userId) {
        SharePrefUtil.putData(PrefsKey.USERID, userId, SharePrefUtil.FileName.DEFAULT_PREFS);
    }

    public static void saveStatus(int status) {
        SharePrefUtil.putData(PrefsKey.STATUS, status, SharePrefUtil.FileName.DEFAULT_PREFS);
    }

    public static void saveUserType(int userType) {
        SharePrefUtil.putData(PrefsKey.USER_TYPE, userType, SharePrefUtil.FileName.DEFAULT_PREFS);
    }

    public static void saveAvatar(String avatarUrl) {
        SharePrefUtil.putData(PrefsKey.AVATAR, avatarUrl, SharePrefUtil.FileName
                .DEFAULT_PREFS);
    }

    public static void saveClassId(String classId) {
        SharePrefUtil.putData(PrefsKey.CLASSID, classId, SharePrefUtil.FileName
                .DEFAULT_PREFS);
    }

    public static void saveIsAlreadyLogin(boolean isAlreadyLogin) {
        SharePrefUtil.putData(PrefsKey.IS_ALREADY_LOGIN, isAlreadyLogin, SharePrefUtil.FileName
                .DEFAULT_PREFS);
    }

    public static void clearUserInfo() {
        NICKNAME = "";
        USERID = "";
        AVATAR = "";
        STATUS = -1;
        USER_TYPE = -1;
        CLASSID = "";
        SESSIONID = "";
        IS_ALREADY_LOGIN = false;

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PrefsKey.NICKNAME, NICKNAME);
        editor.putString(PrefsKey.USERID, USERID);
        editor.putString(PrefsKey.AVATAR, AVATAR);
        editor.putInt(PrefsKey.STATUS, STATUS);
        editor.putInt(PrefsKey.USER_TYPE, USER_TYPE);
        editor.putString(PrefsKey.CLASSID, CLASSID);
        editor.putString(PrefsKey.SESSIONID, SESSIONID);
        editor.putBoolean(PrefsKey.IS_ALREADY_LOGIN, IS_ALREADY_LOGIN);
        editor.apply();
    }

    public static boolean getIsAlreadyLogin() {
        getUserInfo();
        return IS_ALREADY_LOGIN;
    }

    public static String getNickname() {
        getUserInfo();
        return NICKNAME;
    }

    public static String getUserId() {
        getUserInfo();
        return USERID;
    }

    public static String getSessionId() {
        getUserInfo();
        return SESSIONID;
    }

    public static String getAvatar() {
        getUserInfo();
        return AVATAR;
    }

    public static String getClassId() {
        getUserInfo();
        return CLASSID;
    }

    public static int getStatus() {
        getUserInfo();
        return STATUS;
    }

    public static int getUserType() {
        getUserInfo();
        return USER_TYPE;
    }

    public static void getUserInfo() {
        UserInfo.NICKNAME = prefs.getString(PrefsKey.NICKNAME, "");
        UserInfo.USERID = prefs.getString(PrefsKey.USERID, "");
        UserInfo.AVATAR = prefs.getString(PrefsKey.AVATAR, "");
        UserInfo.STATUS = prefs.getInt(PrefsKey.STATUS, -1);
        UserInfo.USER_TYPE = prefs.getInt(PrefsKey.USER_TYPE, -1);
        UserInfo.CLASSID = prefs.getString(PrefsKey.CLASSID, "");
        UserInfo.SESSIONID = prefs.getString(PrefsKey.SESSIONID, "");
        UserInfo.IS_ALREADY_LOGIN = prefs.getBoolean(PrefsKey.IS_ALREADY_LOGIN, false);
    }

}