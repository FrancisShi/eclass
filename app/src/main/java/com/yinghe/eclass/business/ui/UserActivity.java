package com.yinghe.eclass.business.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.yinghe.eclass.R;
import com.yinghe.eclass.business.data.UserInfo;
import com.yinghe.eclass.medium.base.AbsBackActivity;
import com.yinghe.eclass.medium.util.SelectPicUtil;

import butterknife.BindView;
import butterknife.OnClick;

public class UserActivity extends AbsBackActivity {

    private String mPhotoPath;

    @BindView(R.id.user_image)
    ImageView userImageView;

    @BindView(R.id.user_name)
    TextView textViewUserName;

    @OnClick(R.id.activity_user_item_system)
    public void systemClicked() {
        startActivity(new Intent(this, SystemSettingActivity.class));
    }

    @OnClick(R.id.user_image)
    public void userImageClicked(View view) {
        SelectPicUtil.addPic(this);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_user;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("个人中心");

        textViewUserName.setText(UserInfo.getNickname());
    }

}