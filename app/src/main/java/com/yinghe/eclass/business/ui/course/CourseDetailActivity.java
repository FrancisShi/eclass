package com.yinghe.eclass.business.ui.course;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.yinghe.eclass.R;
import com.yinghe.eclass.medium.base.AbsBackActivity;
import com.yinghe.eclass.medium.base.BaseFragment;

import butterknife.BindView;
import butterknife.OnClick;
import me.kaelaela.verticalviewpager.transforms.DefaultTransformer;

public class CourseDetailActivity extends AbsBackActivity {

    public static final String COURSE_ID = "COURSE_ID";
    public static final String COURSE_TITLE = "COURSE_TITLE";
    private static final int PAGER_SIZE = 3;

    private MyPagerAdapter mPagerAdapter;
    private String courseId;

    @BindView(R.id.activity_course_view_pager)
    ViewPager viewPager;

    @OnClick({R.id.course_note, R.id.course_exam, R.id.course_class})
    public void tabClicked(View view) {
        int index = 0;
        switch (view.getId()) {
            case R.id.course_note:
                index = 0;
                break;
            case R.id.course_exam:
                index = 1;
                break;
            case R.id.course_class:
                index = 2;
                break;
            default:
                break;
        }
        viewPager.setCurrentItem(index);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_course_detail;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        courseId = getIntent().getStringExtra(COURSE_ID);
        setTitle(getIntent().getStringExtra(COURSE_TITLE));

        initPager();
    }

    private void initPager() {
        mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        viewPager.setOffscreenPageLimit(PAGER_SIZE);
        viewPager.setAdapter(mPagerAdapter);
        viewPager.setPageTransformer(false, new DefaultTransformer());

        viewPager.setCurrentItem(0);
    }

    private class MyPagerAdapter extends FragmentStatePagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            BaseFragment baseFragment = null;
            switch (position) {
                case 0:
                    baseFragment = CourseNoteFragment.newInstance(courseId);
                    break;
                case 1:
                    baseFragment = CourseExamFragment.newInstance();
                    break;
                case 2:
                    baseFragment = CourseClassFragment.newInstance();
                    break;
                default:
                    break;
            }
            return baseFragment;
        }

        @Override
        public int getCount() {
            return PAGER_SIZE;
        }

    }
}