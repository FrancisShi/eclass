package com.yinghe.eclass.business.entity;

import com.google.gson.annotations.SerializedName;

public class Homework {

    private String id;
    @SerializedName("userName")
    private String username;
    @SerializedName("userId")
    private String userid;
    @SerializedName("homeworkType")
    private int homeworktype;
    private String title;
    private String content;
    @SerializedName("createTime")
    private long createtime;
    @SerializedName("classIds")
    private String classids;
    @SerializedName("xkId")
    private String xkid;
    @SerializedName("schoolId")
    private String schoolid;
    @SerializedName("contentNames")
    private String contentnames;
    @SerializedName("contentPaths")
    private String contentpaths;
    @SerializedName("homeworkScore")
    private int homeworkscore;
    @SerializedName("startTime")
    private long starttime;
    @SerializedName("endTime")
    private String endtime;
    @SerializedName("showAnswer")
    private int showanswer;
    private int timeout;
    private String answer;
    @SerializedName("answerNames")
    private String answernames;
    @SerializedName("answerPaths")
    private String answerpaths;
    @SerializedName("submitCount")
    private int submitcount;
    @SerializedName("markingCount")
    private int markingcount;
    @SerializedName("studentCount")
    private int studentcount;
    @SerializedName("homeWorkAnswer")
    private String homeworkanswer;
    @SerializedName("submitLateCount")
    private String submitlatecount;
    @SerializedName("classAverageScore")
    private int classaveragescore;
    @SerializedName("classStudentCount")
    private String classstudentcount;
    @SerializedName("classSubmitCount")
    private int classsubmitcount;
    @SerializedName("classLateCount")
    private int classlatecount;
    @SerializedName("classMarkCount")
    private int classmarkcount;
    @SerializedName("isSubmit")
    private int issubmit;


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }


    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }


    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUserid() {
        return userid;
    }


    public void setHomeworktype(int homeworktype) {
        this.homeworktype = homeworktype;
    }

    public int getHomeworktype() {
        return homeworktype;
    }


    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }


    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }


    public void setCreatetime(int createtime) {
        this.createtime = createtime;
    }

    public long getCreatetime() {
        return createtime;
    }


    public void setClassids(String classids) {
        this.classids = classids;
    }

    public String getClassids() {
        return classids;
    }


    public void setXkid(String xkid) {
        this.xkid = xkid;
    }

    public String getXkid() {
        return xkid;
    }


    public void setSchoolid(String schoolid) {
        this.schoolid = schoolid;
    }

    public String getSchoolid() {
        return schoolid;
    }


    public void setContentnames(String contentnames) {
        this.contentnames = contentnames;
    }

    public String getContentnames() {
        return contentnames;
    }


    public void setContentpaths(String contentpaths) {
        this.contentpaths = contentpaths;
    }

    public String getContentpaths() {
        return contentpaths;
    }


    public void setHomeworkscore(int homeworkscore) {
        this.homeworkscore = homeworkscore;
    }

    public int getHomeworkscore() {
        return homeworkscore;
    }


    public void setStarttime(int starttime) {
        this.starttime = starttime;
    }

    public long getStarttime() {
        return starttime;
    }


    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getEndtime() {
        return endtime;
    }


    public void setShowanswer(int showanswer) {
        this.showanswer = showanswer;
    }

    public int getShowanswer() {
        return showanswer;
    }


    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getTimeout() {
        return timeout;
    }


    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }


    public void setAnswernames(String answernames) {
        this.answernames = answernames;
    }

    public String getAnswernames() {
        return answernames;
    }


    public void setAnswerpaths(String answerpaths) {
        this.answerpaths = answerpaths;
    }

    public String getAnswerpaths() {
        return answerpaths;
    }


    public void setSubmitcount(int submitcount) {
        this.submitcount = submitcount;
    }

    public int getSubmitcount() {
        return submitcount;
    }


    public void setMarkingcount(int markingcount) {
        this.markingcount = markingcount;
    }

    public int getMarkingcount() {
        return markingcount;
    }


    public void setStudentcount(int studentcount) {
        this.studentcount = studentcount;
    }

    public int getStudentcount() {
        return studentcount;
    }


    public void setHomeworkanswer(String homeworkanswer) {
        this.homeworkanswer = homeworkanswer;
    }

    public String getHomeworkanswer() {
        return homeworkanswer;
    }


    public void setSubmitlatecount(String submitlatecount) {
        this.submitlatecount = submitlatecount;
    }

    public String getSubmitlatecount() {
        return submitlatecount;
    }


    public void setClassaveragescore(int classaveragescore) {
        this.classaveragescore = classaveragescore;
    }

    public int getClassaveragescore() {
        return classaveragescore;
    }


    public void setClassstudentcount(String classstudentcount) {
        this.classstudentcount = classstudentcount;
    }

    public String getClassstudentcount() {
        return classstudentcount;
    }


    public void setClasssubmitcount(int classsubmitcount) {
        this.classsubmitcount = classsubmitcount;
    }

    public int getClasssubmitcount() {
        return classsubmitcount;
    }


    public void setClasslatecount(int classlatecount) {
        this.classlatecount = classlatecount;
    }

    public int getClasslatecount() {
        return classlatecount;
    }


    public void setClassmarkcount(int classmarkcount) {
        this.classmarkcount = classmarkcount;
    }

    public int getClassmarkcount() {
        return classmarkcount;
    }


    public void setIssubmit(int issubmit) {
        this.issubmit = issubmit;
    }

    public int getIssubmit() {
        return issubmit;
    }

}
