package com.yinghe.eclass.business.ui.student;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.yinghe.eclass.R;
import com.yinghe.eclass.business.data.UserInfo;
import com.yinghe.eclass.business.entity.ChooseClassroom;
import com.yinghe.eclass.business.entity.ClassStudent;
import com.yinghe.eclass.business.entity.ClassroomGroup;
import com.yinghe.eclass.medium.base.BaseFragment;
import com.yinghe.eclass.medium.http.BaseSubscriber;
import com.yinghe.eclass.medium.http.ProgressSubscriber;
import com.yinghe.eclass.medium.http.RetrofitClient;
import com.yinghe.eclass.medium.http.SubscriberOnNextListener;
import com.yinghe.eclass.medium.util.ViewHolderUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class StudentClassFragment extends BaseFragment {

    public static final String STUDENT_TAB_CLICK = "STUDENT_TAB_CLICK";


    @BindView(R.id.student_grid)
    GridView studentGridView;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            checkShowChoose();
        }
    };
    private SubscriberOnNextListener<List<ClassStudent>> studentSubscriberOnNextListener;

    private List<ChooseClassroom> mChooseClassrooms = new ArrayList<>();
    private String mChoosenClassId;

    private SubscriberOnNextListener<List<ChooseClassroom>> studentChooseClassroomSubscribe = new
            SubscriberOnNextListener<List<ChooseClassroom>>() {

                @Override
                public void onNext(List<ChooseClassroom> chooseClassroomList) {
                    mChooseClassrooms = chooseClassroomList;
                }
            };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(STUDENT_TAB_CLICK);
        mActivity.registerReceiver(mReceiver, intentFilter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mActivity.unregisterReceiver(mReceiver);
    }

    @Override
    public int getContentViewId() {
        return R.layout.fragment_student_class;
    }

    public static StudentClassFragment newInstance() {

        Bundle args = new Bundle();

        StudentClassFragment fragment = new StudentClassFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        requestClass();
    }

    private void requestClass() {
        BaseSubscriber<List<ChooseClassroom>> baseSubscriber = new
                BaseSubscriber<>(studentChooseClassroomSubscribe, mActivity);

        RetrofitClient.getInstance().chooseClassroom(baseSubscriber, UserInfo.getUserId());

    }

    public void checkShowChoose() {
        // TODO: 16/6/19
        if (mChoosenClassId == null) {
            showChoose();
        }else {
            requestStudent();
        }
    }

    private void showChoose() {
        // TODO: 16/6/19
        StudentChooseClassFragment dialogFragment = new StudentChooseClassFragment();
        dialogFragment.setChooseClassrooms(mChooseClassrooms);
        dialogFragment.setChooseListener(new StudentChooseClassFragment.ChooseListener() {
            @Override
            public void chooseItem(String id) {
                mChoosenClassId = id;

                requestStudent();
            }
        });
        dialogFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialogFragment.show(mActivity.getFragmentManager(), "dialog");
    }

    private void requestStudent() {
        studentSubscriberOnNextListener = new
                SubscriberOnNextListener<List<ClassStudent>>() {
                    @Override
                    public void onNext(List<ClassStudent> studentList) {
                        StudentAdapter adapter = new StudentAdapter();
                        adapter.setStudentList(studentList);
                        studentGridView.setAdapter(adapter);
                    }
                };
        RetrofitClient.getInstance().requestStudent(new ProgressSubscriber<List<ClassStudent>>
                (studentSubscriberOnNextListener, mActivity), mChoosenClassId);
    }

    class StudentAdapter extends BaseAdapter{

        private List<ClassStudent> mStudentList;

        private void setStudentList(List<ClassStudent> studentList){
            this.mStudentList = studentList;
        }

        @Override
        public int getCount() {
            return mStudentList == null ? 0 : mStudentList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null){
                convertView = View.inflate(mActivity,R.layout.student_item,null);
            }
            ClassStudent student = mStudentList.get(position);

            TextView name = ViewHolderUtil.get(convertView,R.id.student_item_name);
            name.setText(student.getStudentname());

            return convertView;
        }
    }
}