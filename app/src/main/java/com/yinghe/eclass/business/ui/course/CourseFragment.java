package com.yinghe.eclass.business.ui.course;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yinghe.eclass.R;
import com.yinghe.eclass.business.data.UserInfo;
import com.yinghe.eclass.business.entity.CourseCat;
import com.yinghe.eclass.medium.base.BaseFragment;
import com.yinghe.eclass.medium.http.ProgressSubscriber;
import com.yinghe.eclass.medium.http.RetrofitClient;
import com.yinghe.eclass.medium.http.SubscriberOnNextListener;
import com.yinghe.eclass.medium.util.ViewHolderUtil;

import java.util.List;

import butterknife.BindView;

public class CourseFragment extends BaseFragment {

    SubscriberOnNextListener<List<CourseCat>> courseSubscriberOnNextListener;

    @BindView(R.id.course_cat_container)
    LinearLayout courseCatLinearLayout;

    public static CourseFragment newInstance() {
        CourseFragment fragment = new CourseFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getContentViewId() {
        return R.layout.fragment_course;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        requestCourse();
    }

    private void requestCourse() {
        courseSubscriberOnNextListener = new SubscriberOnNextListener<List<CourseCat>>() {

            @Override
            public void onNext(List<CourseCat> courseCatList) {
                if (courseCatList.size() > 0) {
                    setCourseCatList(courseCatList);
                }
            }
        };
        RetrofitClient.getInstance().course(new ProgressSubscriber<List<CourseCat>>
                (courseSubscriberOnNextListener, mActivity), UserInfo.getUserId());

    }

    private void setCourseCatList(List<CourseCat> courseCatList) {
        courseCatLinearLayout.removeAllViews();

        for (int i = 0; i < courseCatList.size(); i++) {

            View catItemView = View.inflate(mActivity, R.layout.fragment_course_cat_item,
                    null);
            TextView textView = (TextView) catItemView.findViewById(R.id
                    .fragment_cat_item_course_cat_name);

            GridView gridView = (GridView) catItemView.findViewById(R.id
                    .fragment_cat_item_course_cat_grid);

            CourseCat courseItem = courseCatList.get(i);
            textView.setText(courseItem.getXkName());

            CourseAdapter courseAdapter = new CourseAdapter();
            courseAdapter.setCourseList(courseItem.getCourses());
            gridView.setAdapter(courseAdapter);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String courseId = view.getContentDescription().toString();
                    Intent intent = new Intent(mActivity, CourseDetailActivity.class);
                    intent.putExtra(CourseDetailActivity.COURSE_ID, courseId);
                    TextView nameTextView = (TextView) view.findViewById(R.id
                            .course_item_name);
                    intent.putExtra(CourseDetailActivity.COURSE_TITLE, nameTextView.getText()
                            .toString());
                    startActivity(intent);
                }
            });

            courseCatLinearLayout.addView(catItemView);
        }
    }

    class CourseAdapter extends BaseAdapter {

        private List<CourseCat.Course> courseList;

        public void setCourseList(List<CourseCat.Course> courseList) {
            this.courseList = courseList;
        }

        @Override
        public int getCount() {
            return courseList == null ? 0 : courseList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mActivity.getLayoutInflater().inflate(R.layout
                        .fragment_course_grid_item, parent, false);
            }
            TextView textView = ViewHolderUtil.get(convertView, R.id.course_item_name);
            textView.setText(courseList.get(position).getName());
            convertView.setContentDescription(courseList.get(position).getId());
            return convertView;
        }
    }
}
