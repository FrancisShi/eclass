package com.yinghe.eclass.business.ui.student;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.yinghe.eclass.R;
import com.yinghe.eclass.business.entity.ChooseClassroom;
import com.yinghe.eclass.medium.util.ViewHolderUtil;

import java.util.List;

public class StudentChooseClassFragment extends DialogFragment {

    private ListView mListView;
    private List<ChooseClassroom> mChooseClassrooms;
    private ChooseListener mChooseListener;

    public void setChooseClassrooms(List<ChooseClassroom> chooseClassrooms) {
        this.mChooseClassrooms = chooseClassrooms;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_student_choose_class, container, false);

        mListView = (ListView) v.findViewById(R.id.student_choose_class_list);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListView.setAdapter(new ListViewAdapter());
    }

    class ListViewAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mChooseClassrooms == null ? 0 : mChooseClassrooms.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.choose_class_list_item, null);
            }

            final ChooseClassroom item = mChooseClassrooms.get(position);

            TextView name = ViewHolderUtil.get(convertView, R.id.choose_class_item_name);
            name.setText(item.getName());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mChooseListener != null){
                        mChooseListener.chooseItem(item.getId());
                    }
                }
            });
            return convertView;
        }
    }

    public void setChooseListener(ChooseListener chooseListener) {
        this.mChooseListener = chooseListener;
    }

    public interface ChooseListener {
        void chooseItem(String id);
    }
}
