package com.yinghe.eclass.business.ui.student;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.yinghe.eclass.R;
import com.yinghe.eclass.business.ui.classroom.ClassroomGroupFragment;
import com.yinghe.eclass.business.ui.classroom.ClassroomPrideFragment;
import com.yinghe.eclass.medium.base.BaseFragment;

import butterknife.BindView;
import butterknife.OnClick;
import me.kaelaela.verticalviewpager.transforms.DefaultTransformer;

/**
 *  * description
 *  *
 *  * @author Francis @Hangzhou Youzan Technology Co.Ltd
 *  * @date 16/6/19
 *  
 */
public class StudentFragment extends BaseFragment {

    private static final int PAGER_SIZE = 4;
    private StudentPagerAdapter mPagerAdapter;

    private int chosenClass = -1;

    @BindView(R.id.fragment_student_view_pager)
    ViewPager viewPager;

    @OnClick({R.id.student_classroom, R.id.student_pride, R.id.student_group, R
            .id.student_projection})
    public void tabClicked(View view) {
        int index = 0;
        switch (view.getId()) {
            case R.id.student_classroom:
                index = 0;
                break;
            case R.id.student_pride:
                index = 1;
                break;
            case R.id.student_group:
                index = 2;
                break;
            case R.id.student_projection:
                index = 3;
                break;
            default:
                break;
        }
        viewPager.setCurrentItem(index);
    }

    @Override
    public int getContentViewId() {
        return R.layout.fragment_student;
    }

    public static StudentFragment newInstance() {

        Bundle args = new Bundle();

        StudentFragment fragment = new StudentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initPager();
    }

    private void initPager() {
        mPagerAdapter = new StudentPagerAdapter(getFragmentManager());
        viewPager.setOffscreenPageLimit(PAGER_SIZE);
        viewPager.setAdapter(mPagerAdapter);
        viewPager.setPageTransformer(false, new DefaultTransformer());

        viewPager.setCurrentItem(0);
    }

    private class StudentPagerAdapter extends FragmentStatePagerAdapter {

        public StudentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            BaseFragment baseFragment = null;
            switch (position) {
                case 0:
                    baseFragment = StudentClassFragment.newInstance();
                    break;
                case 1:
                    baseFragment = ClassroomPrideFragment.newInstance();
                    break;
                case 2:
                    baseFragment = ClassroomGroupFragment.newInstance();
                    break;
                case 3:
                    baseFragment = StudentProjectionFragment.newInstance();
                    break;
                default:
                    break;
            }
            return baseFragment;
        }

        @Override
        public int getCount() {
            return PAGER_SIZE;
        }

    }

    public int getChoosenClass() {
        return chosenClass;
    }
}
