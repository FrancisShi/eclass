package com.yinghe.eclass.business.ui.classroom;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.yinghe.eclass.R;
import com.yinghe.eclass.business.data.UserInfo;
import com.yinghe.eclass.business.entity.ClassroomPride;
import com.yinghe.eclass.business.entity.ClassroomSelfStatic;
import com.yinghe.eclass.medium.base.BaseFragment;
import com.yinghe.eclass.medium.http.ProgressSubscriber;
import com.yinghe.eclass.medium.http.RetrofitClient;
import com.yinghe.eclass.medium.http.SubscriberOnNextListener;
import com.yinghe.eclass.medium.util.ViewHolderUtil;

import java.util.List;

import butterknife.BindView;

public class ClassroomPrideFragment extends BaseFragment {

    @BindView(R.id.classroom_pride_list_view)
    ListView listView;

    SubscriberOnNextListener<ClassroomSelfStatic> classroomSelfPrideStaticSubscriberOnNextListener;
    SubscriberOnNextListener<List<ClassroomPride>> classroomPrideSubscriberOnNextListener;
    private View header;
    private ClassroomPrideListAdapter listAdapter;

    @Override
    public int getContentViewId() {
        return R.layout.fragment_classroom_pride;
    }

    public static ClassroomPrideFragment newInstance() {

        Bundle args = new Bundle();

        ClassroomPrideFragment fragment = new ClassroomPrideFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listAdapter = new ClassroomPrideListAdapter();

        header = View.inflate(mActivity, R.layout
                        .fragment_classroom_pride_header,
                null);
        listView.addHeaderView(header);
        listView.setAdapter(listAdapter);

        requestPride();
        requestSelfPrideStatic();
    }

    class ClassroomPrideListAdapter extends BaseAdapter {

        private List<ClassroomPride> prideList;

        public void setPrideList(List<ClassroomPride> prideList) {
            this.prideList = prideList;
        }

        @Override
        public int getCount() {
            return prideList == null ? 0 : prideList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mActivity.getLayoutInflater().inflate(R.layout
                        .fragment_classroom_pride_list_item, parent, false);
            }

            ImageView rankeImage = ViewHolderUtil.get(convertView, R.id
                    .classroom_pride_list_item_rank);
            ImageView orderImage = ViewHolderUtil.get(convertView, R.id
                    .classroom_pride_list_item_order);
            rankeImage.setVisibility(View.GONE);

            TextDrawable drawable = TextDrawable.builder()
                    .beginConfig()
                    .textColor(Color.WHITE)
                    .endConfig()
                    .buildRound(position + 1 + "", Color.GRAY);
            if (position == 0) {
                rankeImage.setVisibility(View.VISIBLE);
                rankeImage.setImageDrawable(mActivity.getResources().getDrawable(R.drawable
                        .rank1st));
                drawable = TextDrawable.builder()
                        .beginConfig()
                        .textColor(Color.WHITE)
                        .endConfig()
                        .buildRound(position + 1 + "", mActivity.getResources().getColor(R.color
                                .view_action_red));
            } else if (position == 1) {
                rankeImage.setVisibility(View.VISIBLE);
                rankeImage.setImageDrawable(mActivity.getResources().getDrawable(R.drawable
                        .rank2nd));
                drawable = TextDrawable.builder()
                        .beginConfig()
                        .textColor(Color.WHITE)
                        .endConfig()
                        .buildRound(position + 1 + "", mActivity.getResources().getColor(R.color
                                .view_action_red_pressed));
            } else if (position == 2) {
                rankeImage.setVisibility(View.VISIBLE);
                rankeImage.setImageDrawable(mActivity.getResources().getDrawable(R.drawable
                        .rank3rd));
                drawable = TextDrawable.builder()
                        .beginConfig()
                        .textColor(Color.WHITE)
                        .endConfig()
                        .buildRound(position + 1 + "", mActivity.getResources().getColor(R.color
                                .view_action_red_disable));
            }
            orderImage.setImageDrawable(drawable);

            TextView userNameTextView = ViewHolderUtil.get(convertView, R.id
                    .classroom_pride_list_item_name);
            userNameTextView.setText(prideList.get(position).getStudentname());

            TextView countTextView = ViewHolderUtil.get(convertView, R.id
                    .classroom_pride_list_item_count);
            countTextView.setText(prideList.get(position).getPraisenum() + "颗");

            return convertView;
        }
    }

    private void requestPride() {
        classroomPrideSubscriberOnNextListener = new
                SubscriberOnNextListener<List<ClassroomPride>>() {

                    @Override
                    public void onNext(List<ClassroomPride> prideList) {
                        if (prideList.size() > 0) {
                            listAdapter.setPrideList(prideList);
                            listAdapter.notifyDataSetChanged();
                        }
                    }
                };
        RetrofitClient.getInstance().classroomPride(new ProgressSubscriber<List<ClassroomPride>>
                (classroomPrideSubscriberOnNextListener, mActivity), UserInfo.getClassId());
    }

    private void requestSelfPrideStatic() {
        classroomSelfPrideStaticSubscriberOnNextListener = new
                SubscriberOnNextListener<ClassroomSelfStatic>() {

                    @Override
                    public void onNext(ClassroomSelfStatic classroomSelfStatic) {
                        TextView totalTextView = (TextView) header.findViewById(R.id
                                .classroom_pride_header_total_pride);
                        TextView todayTextView = (TextView) header.findViewById(R.id
                                .classroom_pride_header_today_pride);
                        todayTextView.setText(classroomSelfStatic.getTodaynum() + "颗");
                        totalTextView.setText(classroomSelfStatic.getTotal() + "颗");

                    }
                };
        RetrofitClient.getInstance().classroomSelf(new ProgressSubscriber<ClassroomSelfStatic>
                (classroomSelfPrideStaticSubscriberOnNextListener, mActivity), UserInfo.getUserId
                ());

    }
}
