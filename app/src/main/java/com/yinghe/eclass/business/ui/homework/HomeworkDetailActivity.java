package com.yinghe.eclass.business.ui.homework;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.yinghe.eclass.R;
import com.yinghe.eclass.business.data.UserInfo;
import com.yinghe.eclass.business.entity.HomeWorkSubmit;
import com.yinghe.eclass.business.entity.HomeworkDetail;
import com.yinghe.eclass.medium.base.AbsBackActivity;
import com.yinghe.eclass.medium.http.ProgressSubscriber;
import com.yinghe.eclass.medium.http.RetrofitClient;
import com.yinghe.eclass.medium.http.SubscriberOnNextListener;
import com.yinghe.eclass.medium.util.FileUtil;

import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;

public class HomeworkDetailActivity extends AbsBackActivity {

    public static final String HOMEWORK_ID = "HOMEWORK_ID";

    private String homeworkId;
    private SubscriberOnNextListener<HomeworkDetail> HomeworkDetailSubscriberOnNextListener;

    private SubscriberOnNextListener<HomeWorkSubmit> submitSubscriberOnNextListener;


    @BindView(R.id.activity_homework_detail_content)
    TextView textViewContent;

    @BindView(R.id.activity_homework_detail_accessory)
    GridView gridViewAccessory;

    @BindView(R.id.activity_homework_detail_button)
    Button buttonSubmit;

    @OnClick(R.id.activity_homework_detail_button)
    public void submitClick() {
        final File file = new File("/sdcard/test.doc");
        if (file.isFile()) {
           new Thread(new Runnable() {
               @Override
               public void run() {
                   uploadFile(file);
               }
           }).start();
        }
    }

    private void uploadFile(File file)  {
        try {
            FileUtil.upload("http://121.42.139.218:8188/campus_maven/api/client/file/upload",file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getLayout() {
        return R.layout.activity_homework_detail;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeworkId = getIntent().getStringExtra(HOMEWORK_ID);
        setTitle(getIntent().getStringExtra("title"));
        requestDetail();
    }

    private void requestDetail() {
        HomeworkDetailSubscriberOnNextListener = new
                SubscriberOnNextListener<HomeworkDetail>() {
                    @Override
                    public void onNext(HomeworkDetail homeworkDetail) {
                        textViewContent.setText(Html.fromHtml(homeworkDetail.getContent()));
                        if (homeworkDetail.getIssubmit() == 0){
                            buttonSubmit.setText("上传答案");
                        }
                    }
                };
        // TODO: 16/6/7 add class Id
        RetrofitClient.getInstance().homeworkDetail(new ProgressSubscriber<HomeworkDetail>
                (HomeworkDetailSubscriberOnNextListener, this), UserInfo.getUserId(), homeworkId);
    }
}
